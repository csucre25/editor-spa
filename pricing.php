<?php 
    session_start();  
 ?>
<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Staging Template">
    <meta name="keywords" content="Staging, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title id="title-header">Ecotinta | Membresía</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Aldrich&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <link rel="stylesheet" href="css/stylecardpay.css" type="text/css">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Offcanvas Menu Begin -->
    <div class="offcanvas-menu-overlay"></div>
    <div class="offcanvas-menu-wrapper">
        <div class="offcanvas__logo">
            <a href="#" ><img id="logo-loading" src="img/logo.png" alt=""></a>
        </div>
        <div id="mobile-menu-wrap"></div>
        <div class="offcanvas__widget">
            <a href="./editor.php" class="primary-btn">Probar Editor!</a>
        </div>
    </div>
    <!-- Offcanvas Menu End -->

   <!-- Header Section Begin -->
    <header class="header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3">
                    <div class="header__logo">
                        <a href="#"><img id="logo-header" src="img/logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <nav class="header__menu mobile-menu">
                        <ul>
                            <li><a href="./home">Inicio</a></li>
                            <!--<li><a href="#Nosotros">Nosotros</a></li>-->
                            <li  class="active"><a href="./pricing">Membresía</a></li>
                            <!--<li><a href="#">Pages</a>
                                <ul class="dropdown">
                                    <li><a href="./Project-details.html">Project Details</a></li>
                                    <li><a href="./about.html">About</a></li>
                                    <li><a href="./services.html">Services</a></li>
                                    <li><a href="./blog-details.html">Blog Details</a></li>
                                </ul>
                              
                            </li>--> 
                            <li><a href="./index">Editor</a></li>
                            <!--<li><a href="./contact.html">Contacto</a></li>--> 
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-3 set-bg">
                    <div class="header__widget" id="containerSession">
                        <!--<h4>Carlos Andres Sucre Gamboa</h4>-->
                        <!--<a href="#" class="primary-btn">Probar Editor!</a>-->
                    </div>
                </div>
            </div>
            <div class="canvas__open"><i class="fa fa-bars"></i></div>
        </div>
    </header>
    <!-- Header Section End -->

    <!-- Breadcrumb Section Begin -->
    <div class="breadcrumb-option spad set-bg" data-setbg="img/slide/bg03.jpeg" style="background-position-y: center;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Nuestros Planes</h2>
                        <div class="breadcrumb__links">
                            <a href="./index.html">Home</a>
                            <span>Membresía</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Section End -->

    <!-- Project Page Section Begin -->
    <section class="project-page spad pricing py-5">
        <div class="container">
            <div class="row" id="container-cards-price">
                <!--Inicio cards de precios-->
                 <!-- Free Tier -->
                <div class="col-lg-4">
                    <div class="card mb-5 mb-lg-0">
                    <div class="card-body">
                        <h5 class="card-title text-muted text-uppercase text-center">Free</h5>
                        <h6 class="card-price text-center">$30<span class="period">/ 30 Ptos</span></h6>
                        <hr>
                        <ul class="fa-ul">
                        <li><span class="fa-li"><i class="icon_check"></i></span>Single User</li>
                        <li><span class="fa-li"><i class="icon_check"></i></span>5GB Storage</li>
                        </ul>
                        <a href="#" class="btn btn-block btn-dark text-uppercase" data-toggle="modal" data-target="#exampleModal">COMPRAR</a>
                    </div>
                    </div>
                </div>
                <!-- Plus Tier -->
                <div class="col-lg-4">
                    <div class="card mb-5 mb-lg-0">
                    <div class="card-body">
                        <h5 class="card-title text-muted text-uppercase text-center">Plus</h5>
                        <h6 class="card-price text-center">$50<span class="period">/ 55 Ptos</span></h6>
                        <hr>
                        <ul class="fa-ul">
                        <li><span class="fa-li"><i class="icon_check"></i></span><strong>5 Users</strong></li>
                        <li><span class="fa-li"><i class="icon_check"></i></span>50GB Storage</li>
                        <li><span class="fa-li"><i class="icon_check"></i></span>Unlimited Public Projects</li>
                        </ul>
                        <a href="#" class="btn btn-block btn-dark text-uppercase" data-toggle="modal" data-target="#exampleModal">COMPRAR</a>
                    </div>
                    </div>
                </div>
                <!-- Pro Tier -->
                <div class="col-lg-4">
                    <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-muted text-uppercase text-center">Pro</h5>
                        <h6 class="card-price text-center">$100<span class="period">/ 110 Ptos</span></h6>
                        <hr>
                        <ul class="fa-ul">
                        <li><span class="fa-li"><i class="icon_check"></i></span><strong>Unlimited Users</strong></li>
                        </ul>
                        <a href="#" class="btn btn-block btn-dark text-uppercase" data-toggle="modal" data-target="#exampleModal">COMPRAR</a>
                    </div>
                    </div>
                </div>
                
                 <!--Fin cards de precios-->
                <!--<div class="col-lg-12 text-center">
                    <a href="#" class="primary-btn normal-btn">Load More</a>
                </div>
                -->
            </div>
        </div>
    </section>
    <!-- Project Page Section End -->

     <!-- Footer Section Begin -->
    <footer class="footer set-bg" data-setbg="img/slide/bg07.jpg">
        <div class="container">
            <!--<div class="footer__top">
                <div class="row">
                    <div class="col-lg-8 col-md-6">
                        <div class="footer__top__text">
                            <h2>Ready To Work With Us?</h2>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="footer__top__newslatter">
                            <form action="#">
                                <input type="text" placeholder="Enter your email...">
                                <button type="submit"><i class="fa fa-send"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>-->
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer__about">
                        <div class="footer__logo">
                            <a href="#"><img id="logo-footer" src="img/logo.png" alt=""></a>
                        </div>
                        <ul>
                            <li>collage.editor@gmail.com</li>
                            <li>+51 972 621 077</li>
                        </ul>
                        <!--<div class="footer__social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                        </div>-->
                    </div>
                </div>
                <!--<div class="col-lg-2 offset-lg-1 col-md-3 col-sm-6">
                    <div class="footer__widget">
                        <h6>Company</h6>
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Services</a></li>
                            <li><a href="#">Our Works</a></li>
                            <li><a href="#">Career</a></li>
                            <li><a href="#">FAQs</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <div class="footer__widget">
                        <h6>Services</h6>
                        <ul>
                            <li><a href="#">Architecture</a></li>
                            <li><a href="#">Interior Design</a></li>
                            <li><a href="#">Exterior Design</a></li>
                            <li><a href="#">Planning</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="footer__address">
                        <h6>Get In Touch</h6>
                        <p>7176 Blue Spring Lane <br />Santa Monica, CA 90403</p>
                        <ul>
                            <li>Info.colorlib@gmail.com</li>
                            <li>+84 123 456 789</li>
                        </ul>
                    </div>
                </div>-->
            </div>
            <!--<div class="copyright">
                <div class="row">
                    <div class="col-lg-8 col-md-7">
                        <div class="copyright__text">
                            <p>Copyright © <script>
                                document.write(new Date().getFullYear());
                            </script> All rights reserved | This template is made with <i class="fa fa-heart-o"
                            aria-hidden="true"></i> by <a href="https://colorlib.com"
                            target="_blank">Colorlib</a>
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5">
                    <div class="copyright__widget">
                        <a href="#">Terms of use</a>
                        <a href="#">Privacy Policy</a>
                    </div>
                </div>
            </div>-->
        </div>
    </div>
</footer>
<!-- Footer Section End -->

<!-- Modal Buy Section Begin-->
<div class="modal fade bd-example-modal-lg" id="buyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg set-bg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Información de Pago</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="container-card"> 
  <div class="col1">
    <div class="cardpay">
      <div class="front">
        <div class="type">
          <img class="bankid"/>
        </div>
        <span class="chip"></span>
        <span class="card_number">&#x25CF;&#x25CF;&#x25CF;&#x25CF; &#x25CF;&#x25CF;&#x25CF;&#x25CF; &#x25CF;&#x25CF;&#x25CF;&#x25CF; &#x25CF;&#x25CF;&#x25CF;&#x25CF; </span>
        <div class="date"><span class="date_value">MM / YYYY</span></div>
        <span class="fullname">FULL NAME</span>
      </div>
      <div class="back">
        <div class="magnetic"></div>
        <div class="bar"></div>
        <span class="seccode">&#x25CF;&#x25CF;&#x25CF;</span>
        <span class="chip"></span><span class="disclaimer">This card is property of Random Bank of Random corporation. <br> If found please return to Random Bank of Random corporation - 21968 Paris, Verdi Street, 34 </span>
      </div>
    </div>
  </div>
  <div class="col2">
    <label>Documento</label>
    <input class=""  id="documentoCard" type="text" ng-model="dni" maxlength="8" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
    <label>Titular de la tarjeta</label>
    <input class="inputname" id="titularCard" type="text" placeholder=""/>
    <label>Numero de la tarjeta</label>
    <input class="number" id="numberCard" type="text" ng-model="ncard" maxlength="19" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
    <label>Fecha de vencimiento</label>
    <input class="expire" id="dateCard" type="text" placeholder="MM / YYYY"/>
    <label>CVC</label>
    <input class="ccv" type="text" id="ccvCard" placeholder="CVC" maxlength="3" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
    <label>Monto</label>
    <input class="" id="Monto" type="text" placeholder="Monto" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
	
    <label>Puntos</label>
    <input class="" id="Puntos" type="text" placeholder="Puntos" onkeypress='return event.charCode >= 48 && event.charCode <= 57' disabled/>
    <button class="buy" onclick="registrarVenta()"><!--<i class="material-icons">lock</i> -->Comprar</button>
  </div>
</div>
      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>-->
    </div>
  </div>
</div>
<!-- Modal Buy Section End-->
<!--Begin Modal Login-->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header border-bottom-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-title text-center">
          <h4>Ingreso</h4>
        </div>
        <div class="d-flex flex-column text-center">
          <form>
            <div class="form-group">
              <input type="email" class="form-control" id="txtCorreoL"placeholder="Correo">
            </div>
            <div class="form-group">
              <input type="password" class="form-control" id="txtClaveL" placeholder="Contraseña">
            </div>
            <button type="button" class="btn btn-success btn-block btn-round" onclick="login()">Ingresar</button>
          </form>
          
          <div class="text-center text-muted delimiter"><!--or use a social network--></div>
            
        </div>
      </div>
      
      <div class="modal-footer d-flex justify-content-center">
        <div class="signup-section">¿Aun no te has registrado? <a href="#" onclick="RegistrarUsuario()" class="text-info"> Registrate</a>.</div>
      </div>
    </div>
  </div>
</div>
<!--End Modal Login-->

<!-- Begin Modal Register-->
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered " role="document">
    <div class="modal-content">
      <div class="modal-header border-bottom-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-title text-center">
          <h4>Registro</h4>
        </div>
        <div class="d-flex flex-column text-center">
            <form>
                <div class="form-group">
                    <input type="text" class="form-control" id="txtDocumento" maxlength="8" onkeypress='return event.charCode >= 48 && event.charCode <= 57' placeholder="Nro. Dni">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="txtNombres"placeholder="Nombre Completo">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="txtCmp"placeholder="CMP" maxlength="6" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="txtCiudad"placeholder="Ciudad">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="txtModelo"placeholder="Modelo de Ecógrafo">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="txtMarca"placeholder="Marca de Ecógrafo">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="txtCorreo" placeholder="Correo">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="txtClave" placeholder="Contraseña">
                </div>                
                <div class="form-group">
                    <input type="password" class="form-control" id="txtClaveReingreso" placeholder="Repita Contraseña">
                </div>                              
                <div class="form-group">
                    <input type="text" class="form-control" id="txtTelefono" placeholder="Telefono(Opcional)*" maxlength="9" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                </div>
                <button type="button" class="btn btn-success btn-block btn-round" onclick="registrar()">Registrar</button>
            </form>
          
          <div class="text-center text-muted delimiter"><!--or use a social network--></div>
          <div class="d-flex justify-content-center social-buttons">
            <!--<button type="button" class="btn btn-secondary btn-round" data-toggle="tooltip" data-placement="top" title="Twitter">
              <i class="fab fa-twitter"></i>
            </button>
            <button type="button" class="btn btn-secondary btn-round" data-toggle="tooltip" data-placement="top" title="Facebook">
              <i class="fab fa-facebook"></i>
            </button>
            <button type="button" class="btn btn-secondary btn-round" data-toggle="tooltip" data-placement="top" title="Linkedin">
              <i class="fab fa-linkedin"></i>
            </button>-->
          </div>
        </div>
      </div>
      
      <div class="modal-footer d-flex justify-content-center">
        <div class="signup-section"><!--¿Aun no te has registrado? <a href="#" onnclick="RegistrarUsuario()" class="text-info"> Registrate</a>.--></div>
      </div>
    </div>
  </div>
</div>
<!--End Modal Register-->

<!-- Js Plugins -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.slicknav.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/main.js"></script>
<!-- Incluye Culqi Checkout en tu sitio web-->
<script src="https://checkout.culqi.com/js/v3"></script>
<script src="js/jscardpay.js"></script>
<script src="js/pricing.js"></script>
</body>

</html>