<?php
session_start();

include_once( "../../config/conexion.php");

class Steps_model extends Conexion{
    private $param = array();
    public $con;

    public function __constcodInternot(){
      parent::__constcodInternot();
    }

    public function crud($param){
        $this->param = $param;
        switch ($this->param['opcion'])
        {
            case 'listSteps':
                echo $this->listarSteps();
                break;
            case 'insertSteps':
            	echo $this->insertarSteps();
                break;
            case 'updateSteps':
                echo $this->actualizarSteps();
                break;
            case 'deleteSteps':
                echo $this->eliminarSteps();
                break;
        }
    }

    private function listarSteps(){
    	$sql="SELECT *
            from steps s where activo = 1 order by s.orden ";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);  
    }

    private function insertarSteps(){
        $Titulo=$this->param["Titulo"];
        $Descripcion=$this->param["Descripcion"];
        $Orden=$this->param["Orden"];
        $File=$this->param["File"];
        $uploadedFile = '';
        $Url='';
        if(!empty($File["type"])){
            $fileName = time().'_'.$File['name'];
            $valid_extensions = array("jpeg", "jpg", "png");
            $temporary = explode(".", $File["name"]);
            $file_extension = end($temporary);
            if((($File["type"] == "image/png") || ($File["type"] == "image/jpg") || ($File["type"] == "image/jpeg")) && in_array($file_extension, $valid_extensions)){
                $sourcePath = $File['tmp_name'];
                $targetPath = "../../img/services/".$fileName;
                if(move_uploaded_file($sourcePath,$targetPath)){
                    $uploadedFile = $fileName;
                    $Url="img/services/".$fileName;
                }
            }
        }
        
        $stmt;
        $sql="INSERT steps(orden,url,titulo,descripcion) values(?,?,?,?)";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute([$Orden,$Url,$Titulo,$Descripcion]);
        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }   
    }    

    private function addPuntos(){
        $Puntos=$this->param["Puntos"];
        $idCliente=$this->param["idCliente"];
        $sql="UPDATE cliente SET puntos=puntos + $Puntos  WHERE idCliente=$idCliente";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }
    }   

    private function actualizarSteps(){
        $idStep=$this->param["idStep"];
        $Titulo=$this->param["Titulo"];
        $Descripcion=$this->param["Descripcion"];
        $Orden=$this->param["Orden"];
        $File=$this->param["File"];
        $uploadedFile = '';
        $Url='';
        $sql="UPDATE steps set orden='$Orden',titulo='$Titulo',descripcion='$Descripcion' where idstep=$idStep";
        if(!empty($File["type"])){
            $fileName = time().'_'.$File['name'];
            $valid_extensions = array("jpeg", "jpg", "png");
            $temporary = explode(".", $File["name"]);
            $file_extension = end($temporary);
            if((($File["type"] == "image/png") || ($File["type"] == "image/jpg") || ($File["type"] == "image/jpeg")) && in_array($file_extension, $valid_extensions)){
                $sourcePath = $File['tmp_name'];
                $targetPath = "../../img/services/".$fileName;
                if(move_uploaded_file($sourcePath,$targetPath)){
                    $uploadedFile = $fileName;
                    $Url="img/services/".$fileName;
                    $sql="UPDATE steps set orden='$Orden',url='$Url',titulo='$Titulo',descripcion='$Descripcion' where idstep=$idStep";
                }
            }
        }
        
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }
        
    }
    private function eliminarSteps(){
    	$idStep=$this->param["idStep"];
        $sql="UPDATE steps set activo=0 where idstep=$idStep";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }
    }
}
?>