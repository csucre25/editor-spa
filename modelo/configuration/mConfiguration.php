<?php
session_start();

include_once( "../../config/conexion.php");

class Configuration_model extends Conexion{
    private $param = array();
    public $con;

    public function __constcodInternot(){
      parent::__constcodInternot();
    }

    public function crud($param){
        $this->param = $param;
        switch ($this->param['opcion'])
        {
            case 'listStep':
                echo $this->listarStep();
                break;
            case 'listConfiguration':
            	echo $this->listarRazon();
                break;
            case 'listConfig':
                echo $this->listarConfig();
                break;
            case 'updateConfig':
                echo $this->actualizarConfig();
                break;
            case 'deleteCliente':
                echo $this->eliminarCliente();
                break;
        }
    }

    private function listarStep(){
    	$sql="SELECT * 
            from steps s where activo = 1 order by orden ";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);  
    }

    private function listarConfig(){
    	$sql="SELECT * 
            from configuration c ";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);  
    }

    private function listarRazon(){
    	$sql="SELECT c.url as video, c.telefono, c.email, c.logo, c.empresa as razon 
            from configuration c ";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);  
    }  

    private function actualizarConfig(){

        $Usuario=$this->param["Usuario"];
        $Nombre=$this->param["Nombre"];
        $Telefono=$this->param["Telefono"];
        $Correo=$this->param["Correo"];
        $Empresa=$this->param["Empresa"];
        $Logo=$this->param["Logo"];
        $Video=$this->param["Video"];

        $urlVideo="";
        $urlLogo="";
        $sql="UPDATE configuration set admin='$Usuario',nombre='$Nombre',telefono='$Telefono',email='$Correo',empresa='$Empresa'";

        if(!empty($Logo["type"])){
            $fileName = time().'_'.$Logo['name'];
            $valid_extensions = array("jpeg", "jpg", "png");
            $temporary = explode(".", $Logo["name"]);
            $file_extension = end($temporary);
            if((($Logo["type"] == "image/png") || ($Logo["type"] == "image/jpg") || ($Logo["type"] == "image/jpeg")) && in_array($file_extension, $valid_extensions)){
                $sourcePath = $Logo['tmp_name'];
                $targetPath = "../../img/logo/".$fileName;
                if(move_uploaded_file($sourcePath,$targetPath)){
                    $uploadedFile = $fileName;
                    $urlLogo="../../img/logo/".$fileName;
                    $sql = $sql.",logo='$urlLogo'";
                }
            }
        }

        if(!empty($Video["type"])){
            $fileName = time().'_'.$Video['name'];
            $valid_extensions = array("mp4");
            $temporary = explode(".", $Video["name"]);
            $file_extension = end($temporary);
            if((($Video["type"] == "video/mp4") || ($Video["type"] == "video/mp4") || ($Video["type"] == "video/mp4")) && in_array($file_extension, $valid_extensions)){
                $sourcePath = $Video['tmp_name'];
                $targetPath = "../../img/video/".$fileName;
                if(move_uploaded_file($sourcePath,$targetPath)){
                    $uploadedFile = $fileName;
                    $urlVideo="../../img/video/".$fileName;
                    $sql = $sql.",url='$urlVideo'";
                }
            }
        }
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }        
    }
    private function eliminarCliente(){
    	$idCliente=$this->param["idCliente"];
        $sql="DELETE FROM cliente WHERE idCliente=$idCliente";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }
    }
}
?>