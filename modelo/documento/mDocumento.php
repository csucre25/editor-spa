<?php
session_start();

include_once( "../../config/conexion.php");

class Documento_model extends Conexion{
    private $param = array();
    public $con;

    public function __constcodInternot(){
      parent::__constcodInternot();
    }

    public function crud($param){
        $this->param = $param;
        switch ($this->param['opcion'])
        {
            case 'listDocumento':
                echo $this->listarDocumento();
                break;
            case 'listDocumentoPendiente':
                echo $this->listarDocumentoPendiente();
                break;
            case 'listDocumentoPendienteByClient':
                echo $this->listarDocumentoByCliente();
                break;
            case 'insertDocumento':
            	echo $this->insertarDocumento();
                break;
            case 'updateDocumento':
                echo $this->actualizarDocumento();
                break;
            case 'deleteDocumento':
                echo $this->eliminarDocumento();
                break;
        }
    }

    private function listarDocumento(){
    	$sql="SELECT d.idDocumento,d.Serie,d.Numero,d.Fecha,d.Estado, d.idTipoPago,d.idCliente,tp.TipoPago, d.Monto, c.RazonSocial
        from documento d 
        inner join tipopago tp on d.idTipoPago=tp.idTipoPago 
        inner join cliente c on c.idCliente = d.idCliente
        order by idDocumento";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);  
    }

    private function listarDocumentoPendiente(){
    	$sql="SELECT d.idDocumento,d.Serie,d.Numero,d.Fecha,d.Estado,
         d.idTipoPago,d.idCliente,tp.TipoPago, d.Monto 
         from documento d 
         inner join tipopago tp 
         on d.idTipoPago=tp.idTipoPago where d.Estado=0 order by idDocumento";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);  
    }

    private function listarDocumentoByCliente(){
        $idCliente=$this->param["idCliente"];
    	$sql="SELECT d.idDocumento,d.Serie,d.Numero,d.Fecha,d.Estado,
         d.idTipoPago,d.idCliente,tp.TipoPago, d.Monto 
         from documento d 
         inner join tipopago tp 
         on d.idTipoPago=tp.idTipoPago where d.Estado=0 and d.idCliente=$idCliente order by idDocumento";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);  
    }


    private function insertarDocumento(){
        $Serie=$this->param["Serie"];
        $Numero=$this->param["Numero"];
        $Fecha=$this->param["Fecha"];
        $idTipoPago=$this->param["idTipoPago"];
        $idCliente=$this->param["idCliente"];
        $Monto=$this->param["Monto"];
        //Transformacion fecha dd/MM/yyyy --> format mysql yyyy.MM.dd
        $DateFecha = strtotime($Fecha); 
        $DateFecha = date('Y-m-d',$DateFecha);
        $sql="SELECT COUNT(*) from documento d WHERE d.Serie='$Serie' and d.Numero='$Numero'";
        $sentencia=$this->conexion_db->query($sql);
         if ($sentencia->fetchColumn()==0) {
            $stmt;
            $sql="INSERT documento(Serie,Numero,Fecha,idTipoPago,idCliente,Monto) values(?,?,?,?,?,?)";
            $stmt= $this->conexion_db->prepare($sql);
            $stmt->execute([$Serie,$Numero,$Fecha,$idTipoPago,$idCliente,$Monto]);
            $this->conexion_db=null;
            return json_encode(1); 
        }    
        else{
            $this->conexion_db=null;
            return json_encode(0); 
        }      
    }    

    private function actualizarDocumento(){
        $idCliente=$this->param["idCliente"];
        $RazonSocial=$this->param["RazonSocial"];
        $Responsanble=$this->param["Responsanble"];
        $RUC=$this->param["RUC"];
        $DNIResponsable=$this->param["DNIResponsable"];
        $idZona=$this->param["idZona"];
        $sql="UPDATE cliente set RazonSocial=$RazonSocial,Responsanble='$Responsanble',RUC='$RUC',DNIResponsable='$DNIResponsable',idZona='$idZona' where idCliente=$idCliente";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }
    }
    private function eliminarDocumento(){
        $idDocumento=$this->param["idDocumento"];

        $sql="SELECT COUNT(*) from documLetra d WHERE d.idDocumento='$idDocumento' ";
        $sentencia=$this->conexion_db->query($sql);
         if ($sentencia->fetchColumn()==0) {

             $sql1="UPDATE documento set Estado= '2', Activado = '0' where idDocumento=$idDocumento";
             $stmt= $this->conexion_db->prepare($sql1);
             $stmt->execute();

              if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
                }else{
            $this->conexion_db=null;
            return json_encode(0); 
              }
         }else{
            $this->conexion_db=null;
            return json_encode(0); 
         }
    

    }
}
?>