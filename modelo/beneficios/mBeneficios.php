<?php
session_start();

include_once( "../../config/conexion.php");

class Beneficios_model extends Conexion{
    private $param = array();
    public $con;

    public function __constcodInternot(){
      parent::__constcodInternot();
    }

    public function crud($param){
        $this->param = $param;
        switch ($this->param['opcion'])
        {
            case 'listBeneficios':
                echo $this->listarBeneficios();
                break;
            case 'listBeneficiosByPricing':
                echo $this->listarBeneficiosByPricing();
                break;
            case 'insertBeneficios':
            	echo $this->insertarBeneficios();
                break;
            case 'updateBeneficios':
                echo $this->actualizarBeneficios();
                break;
            case 'deleteBeneficios':
                echo $this->eliminarBeneficios();
                break;
        }
    }
//select count(idbeneficio) as cont from beneficios where activo=1 group by idprecio
    private function listarBeneficios(){
    	$sql="SELECT b.idbeneficio,b.idprecio,b.beneficio, pp.nombre, pp.precio,pp.puntos,pp.orden 
            FROM beneficios b 
            INNER JOIN pricing_page pp 
                ON pp.idprecio = b.idprecio 
                AND pp.estado=1  
            WHERE b.activo = 1 order by pp.orden,b.idbeneficio";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);  
    }

    private function listarBeneficiosByPricing(){
    	$sql="SELECT COUNT(idbeneficio) AS cont FROM beneficios WHERE activo=1 GROUP BY idprecio";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);  
    }

    private function insertarBeneficios(){
        
        $idPrecio=$this->param["idPrecio"];
        $Beneficio=$this->param["Beneficio"];

        $stmt;
        $sql="INSERT beneficios(idprecio,beneficio) values(?,?)";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute([$idPrecio,$Beneficio]);
        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }   
    }    

    private function actualizarBeneficios(){

        $idPrecio=$this->param["idPrecio"];
        $idBeneficio=$this->param["idBeneficio"];
        $Beneficio=$this->param["Beneficio"];

        $sql="SELECT COUNT(*) from beneficios b WHERE b.idBeneficio = $idBeneficio";
        $sentencia=$this->conexion_db->query($sql);
         if ($sentencia->fetchColumn()==1) {
            $sql="UPDATE beneficios set idPrecio='$idPrecio',beneficio='$Beneficio' where idBeneficio=$idBeneficio";
            $stmt= $this->conexion_db->prepare($sql);
            $stmt->execute();
            if ($stmt->rowCount()>0) {
                $this->conexion_db=null;
                return json_encode(1); 
            }else{
                $this->conexion_db=null;
                return json_encode(0); 
            }
         }else{
            $this->conexion_db=null;
            return json_encode(4); 
        }        
    }

    private function eliminarBeneficios(){
        
        $idBeneficio=$this->param["idBeneficio"];
        
        $sql="UPDATE beneficios SET activo=0 WHERE idBeneficio=$idBeneficio ";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }
    }
}
?>