<?php
session_start();

include_once( "../../config/conexion.php");

class Cliente_model extends Conexion{
    private $param = array();
    public $con;

    public function __constcodInternot(){
      parent::__constcodInternot();
    }

    public function crud($param){
        $this->param = $param;
        switch ($this->param['opcion'])
        {
            case 'listCliente':
                echo $this->listarCliente();
                break;
            case 'insertCliente':
            	echo $this->insertarCliente();
                break;
            case 'updateCliente':
                echo $this->actualizarCliente();
                break;
            case 'addPuntos':
                echo $this->addPuntos();
                break;
            case 'deleteCliente':
                echo $this->eliminarCliente();
                break;
            case 'listClienteBySession':
                echo $this->listarClienteBySession();
                break;
            case 'validateCliente':
                echo $this->validarCliente();
                break;
            case 'initSesion':
                echo $this->inicioSesion();
                break;
            case 'deductPoints':
                echo $this->deductPoints();
                break;
        }
    }

    private function listarCliente(){
    	$sql="SELECT c.idCliente, c.documento, c.nombres, c.cmp, c.ecografo_modelo, c.ecografo_marca, c.ciudad, c.correo, c.telefono, c.puntos 
            from cliente c where activo = 1 order by idCliente ";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);  
    }

    private function insertarCliente(){
        $Documento=$this->param["Documento"];
        $Nombres=$this->param["Nombres"];
        $Cmp=$this->param["Cmp"];
        $Ciudad=$this->param["Ciudad"];
        $Modelo=$this->param["Modelo"];
        $Marca=$this->param["Marca"];
        $Correo=$this->param["Correo"];
        $Clave=$this->param["Clave"];
        $Telefono=$this->param["Telefono"];
        $Puntos=$this->param["Puntos"];
        
        $stmt;
        $sql="INSERT cliente(documento,nombres,cmp,ciudad,ecografo_modelo,ecografo_marca,correo,clave,telefono,puntos) values(?,?,?,?,?,?,?,?,?,?)";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute([$Documento,$Nombres,$Cmp,$Ciudad,$Modelo,$Marca,$Correo,$Clave,$Telefono,$Puntos]);
        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }   
    }    

    private function addPuntos(){
        $Puntos=$this->param["Puntos"];
        $idCliente=$this->param["idClienteM"];
        $sql="UPDATE cliente SET puntos=puntos + $Puntos  WHERE idCliente=$idCliente";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }
    } 
    
    private function deductPoints(){
        $Puntos=3;
        $idCliente=$this->param["idCliente"];
        if($idCliente == "" && isset( $_SESSION['S_Usuario'] ) ){
            $idCliente = $_SESSION['S_IdUsuario'];
        }
        $sql="UPDATE cliente SET puntos=puntos - $Puntos  WHERE idCliente=$idCliente";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }
    }

    private function actualizarCliente(){

        $Documento=$this->param["Documento"];
        $Nombres=$this->param["Nombres"];
        $Cmp=$this->param["Cmp"];
        $Ciudad=$this->param["Ciudad"];
        $Modelo=$this->param["Modelo"];
        $Marca=$this->param["Marca"];
        $Correo=$this->param["Correo"];
        $Clave=$this->param["Clave"];
        $Telefono=$this->param["Telefono"];
        $Puntos=$this->param["Puntos"];
        $idCliente=$this->param["idClienteM"];

        $sql="SELECT COUNT(*) from cliente c WHERE c.RUC='$RUC' and c.idCliente <> $idCliente";
        $sentencia=$this->conexion_db->query($sql);
         if ($sentencia->fetchColumn()==0) {
            $sql="UPDATE cliente set RazonSocial='$RazonSocial',Responsable='$Responsable',RUC='$RUC',DNIResponsable='$DNIResponsable',idZona=$idZona where idCliente=$idCliente";
            $stmt= $this->conexion_db->prepare($sql);
            $stmt->execute();
            if ($stmt->rowCount()>0) {
                $this->conexion_db=null;
                return json_encode(1); 
            }else{
                $this->conexion_db=null;
                return json_encode(0); 
            }
         }else{
            $this->conexion_db=null;
            return json_encode(4); 
        } 

        

        
    }
    private function eliminarCliente(){
    	$idCliente=$this->param["idClienteM"];
        $sql="UPDATE cliente SET activo=0 WHERE idCliente=$idCliente";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }
    }

    private function validarCliente(){
        $idCliente=$this->param["idCliente"];
        if ($idCliente != null ){
            return json_encode(1); 
        }else{
            return json_encode(0); 
        }
    }

    private function inicioSesion(){
		$Correo=$this->param['Correo'];
		$Clave=$this->param['Clave'];
		$sql="SELECT COUNT(*) from cliente c WHERE c.correo='$Correo'
		and c.clave='$Clave' and activo = 1";
		$sentencia=$this->conexion_db->query($sql);
		if ($sentencia->fetchColumn()>0) {
			$sql2="SELECT * from cliente c WHERE c.correo='$Correo'
			and c.clave='$Clave' limit 1";
			$sentencia=$this->conexion_db->query($sql2);
			$sentencia->execute();
       		$resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
			$sentencia->closeCursor();
			$this->conexion_db=null;
			if (count($resultado) == 0) {
				return '4';
			}else{
				foreach ($resultado as $key => $v) {
					$_SESSION['S_IdUsuario']= utf8_encode($v["idcliente"]);
					$_SESSION['S_Usuario']= utf8_encode($v["nombres"]);
					$_SESSION['S_Cargo']= "Cliente";
				}
   				 return json_encode(1);//'1'; 
			}
		}else{
			return '0';
		}
    }
    
    private function listarClienteBySession(){
        $idCliente=$this->param["idCliente"];
        if($idCliente == "" && isset( $_SESSION['S_Usuario'] ) ){
            $idCliente = $_SESSION['S_IdUsuario'];
        }
        if ($idCliente == "admin"){
            $sql="SELECT *
                from cliente c where activo = 1 and idcliente=1  ";
            $sentencia=$this->conexion_db->prepare($sql);
            $sentencia->execute();
            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
            $sentencia->closeCursor();
            $this->conexion_db=null;
            return json_encode($resultado); 
        }else if($idCliente != ""){
            $sql="SELECT *
                from cliente c where activo = 1 and idcliente=$idCliente ";
            $sentencia=$this->conexion_db->prepare($sql);
            $sentencia->execute();
            $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
            $sentencia->closeCursor();
            $this->conexion_db=null;
            return json_encode($resultado);  
        }else {
            return json_encode(0); 
        }
    	
    }
}
?>