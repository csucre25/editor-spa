<?php 
	session_start();
 ?>
<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Staging Template">
    <meta name="keywords" content="Staging, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ecotinta | Editor</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Aldrich&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="js/gridstack/gridstack.min.css">

	<!-- Css Styles -->
	<link rel="stylesheet" href="css/style-editor.css" type="text/css">
	<link rel="stylesheet" href="css/cso/css/style.css" type="text/css"/>
	<link rel="stylesheet" href="css/style-navbar.css" type="text/css">
	<!--script src="https://unpkg.com/jspdf@latest/dist/jspdf.umd.min.js"></script-->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/stylecardpay.css" type="text/css"><link rel="stylesheet" href="css/cropper.css" />
	<script src="https://cdn.jsdelivr.net/gh/jamesssooi/Croppr.js@2.3.0/dist/croppr.js"></script>
    <link href="https://cdn.jsdelivr.net/gh/jamesssooi/Croppr.js@2.3.0/dist/croppr.css" rel="stylesheet"/>
</head>

<body>
<!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>
<!-- Navigation -->
    <section class="navigation" id="navigation_bar">
  <div class="nav-container">
    <div class="brand">
      <a href="#"><img id="logo-header" src="img/logo.png" alt=""></a>
    </div>
    <nav>
      <div class="nav-mobile"><a id="nav-toggle" href="#!"><span></span></a></div>
      <ul class="nav-list">
        <li>
          <a href="./home">Inicio</a>
        </li>
        <li>
          <a href="./pricing">Membresias</a>
        </li>
        <!--<li>
          <a href="#!">Services</a>
          <ul class="nav-dropdown">
            <li>
              <a href="#!">Web Design</a>
            </li>
            <li>
              <a href="#!">Web Development</a>
            </li>
            <li>
              <a href="#!">Graphic Design</a>
            </li>
          </ul>
        </li>-->
        <li>
          <a href="#!" id="btnComprar">Comprar Puntos!</a>
        </li>
        <!--<li>
          <a href="#!">Portfolio</a>
          <ul class="nav-dropdown">
            <li>
              <a href="#!">Web Design</a>
            </li>
            <li>
              <a href="#!">Web Development</a>
            </li>
            <li>
              <a href="#!">Graphic Design</a>
            </li>
          </ul>
        </li>-->
        <li>
          <a href="view/menus/cerrarSesion.php">Cerrar Sesion</a>
        </li>
		
        <li>
        </li>
		
        <li>
          <a href="#" id="txtPtos" style="background: #ffc107;" >Puntos</a>
        </li>
      </ul>
		  <input type="hidden" id="hdPtos">
    </nav>
  </div>
</section>

<!-- Navigation -->

<div class="collage-designer-wrapper" style="height: fit-content !important;">
	<div class="collage-main" style="height: fit-content !important;">
		<!-- v-if is landscape => add class open to toolbar-wrapper on page load -->
		<div class="collage-toolbar toolbar-wrapper" id="collage-toolbar">
			<div class="toolbar-close-icon closed" id="close-toolbar">
				<svg class="toolbar-icon" viewBox="0 0 24 24">
					<path fill="#fff" d="M7.41,8.58L12,13.17L16.59,8.58L18,10L12,16L6,10L7.41,8.58Z" />
				</svg>
			</div>
			<input class="toolbar-button-input" type="radio" name="collage-tools" id="collage-layouts" checked>
			<label class="toolbar-button" for="collage-layouts">
				<svg class="toolbar-icon" viewBox="0 0 24 24">
					<path fill="currentColor" d="M3,11H11V3H3M3,21H11V13H3M13,21H21V13H13M13,3V11H21V3" />
				</svg>
				Plantillas
			</label>
			<input class="toolbar-button-input" type="radio" name="collage-tools" id="collage-images">
			<label class="toolbar-button" for="collage-images">
				<svg class="toolbar-icon" viewBox="0 0 24 24">
					<path fill="currentColor" d="M5,17L9.5,11L13,15.5L15.5,12.5L19,17M20,6H12L10,4H4A2,2 0 0,0 2,6V18A2,2 0 0,0 4,20H20A2,2 0 0,0 22,18V8A2,2 0 0,0 20,6Z" />
				</svg>
				Imagenes
			</label>
			<input class="toolbar-button-input" type="radio" name="collage-tools" id="collage-text">
			<label class="toolbar-button" for="collage-text">
				<svg class="toolbar-icon" viewBox="0 0 24 24">
					<path fill="currentColor" d="M17,8H20V20H21V21H17V20H18V17H14L12.5,20H14V21H10V20H11L17,8M18,9L14.5,16H18V9M5,3H10C11.11,3 12,3.89 12,5V16H9V11H6V16H3V5C3,3.89 3.89,3 5,3M6,5V9H9V5H6Z" />
				</svg>
				Firmas
			</label>
			<!--input class="toolbar-button-input" type="radio" name="collage-tools" id="collage-background">
			<label class="toolbar-button" for="collage-background">
				<svg class="toolbar-icon" viewBox="0 0 24 24">
					<path fill="currentColor" d="M9.29,21H12.12L21,12.12V9.29M19,21C19.55,21 20.05,20.78 20.41,20.41C20.78,20.05 21,19.55 21,19V17L17,21M5,3A2,2 0 0,0 3,5V7L7,3M11.88,3L3,11.88V14.71L14.71,3M19.5,3.08L3.08,19.5C3.17,19.85 3.35,20.16 3.59,20.41C3.84,20.65 4.15,20.83 4.5,20.92L20.93,4.5C20.74,3.8 20.2,3.26 19.5,3.08Z" />
				</svg>
				Fondo
			</label-->
			<input class="toolbar-button-input" type="radio" name="collage-tools" id="collage-background">
			<label class="toolbar-button"  onclick="print_collage()">
				<svg class="toolbar-icon" viewBox="0 0 24 24">
				<path fill="currentColor" d="m21.5 18h-3c-.276 0-.5-.224-.5-.5s.224-.5.5-.5h3c.827 0 1.5-.673 1.5-1.5v-7c0-.827-.673-1.5-1.5-1.5h-19c-.827 0-1.5.673-1.5 1.5v7c0 .827.673 1.5 1.5 1.5h3c.276 0 .5.224.5.5s-.224.5-.5.5h-3c-1.379 0-2.5-1.122-2.5-2.5v-7c0-1.378 1.121-2.5 2.5-2.5h19c1.379 0 2.5 1.122 2.5 2.5v7c0 1.378-1.121 2.5-2.5 2.5z"/><path d="m14.5 21h-6c-.276 0-.5-.224-.5-.5s.224-.5.5-.5h6c.276 0 .5.224.5.5s-.224.5-.5.5z"/>
				<path fill="currentColor" d="m14.5 19h-6c-.276 0-.5-.224-.5-.5s.224-.5.5-.5h6c.276 0 .5.224.5.5s-.224.5-.5.5z"/>
				<path fill="currentColor" d="m10.5 17h-2c-.276 0-.5-.224-.5-.5s.224-.5.5-.5h2c.276 0 .5.224.5.5s-.224.5-.5.5z"/>
				<path fill="currentColor" d="m18.5 7c-.276 0-.5-.224-.5-.5v-4c0-.827-.673-1.5-1.5-1.5h-9c-.827 0-1.5.673-1.5 1.5v4c0 .276-.224.5-.5.5s-.5-.224-.5-.5v-4c0-1.378 1.121-2.5 2.5-2.5h9c1.379 0 2.5 1.122 2.5 2.5v4c0 .276-.224.5-.5.5z"/>
				<path fill="currentColor" d="m16.5 24h-9c-1.379 0-2.5-1.122-2.5-2.5v-8c0-.276.224-.5.5-.5h13c.276 0 .5.224.5.5v8c0 1.378-1.121 2.5-2.5 2.5zm-10.5-10v7.5c0 .827.673 1.5 1.5 1.5h9c.827 0 1.5-.673 1.5-1.5v-7.5z"/>
					<!--<path fill="currentColor" d="M9.29,21H12.12L21,12.12V9.29M19,21C19.55,21 20.05,20.78 20.41,20.41C20.78,20.05 21,19.55 21,19V17L17,21M5,3A2,2 0 0,0 3,5V7L7,3M11.88,3L3,11.88V14.71L14.71,3M19.5,3.08L3.08,19.5C3.17,19.85 3.35,20.16 3.59,20.41C3.84,20.65 4.15,20.83 4.5,20.92L20.93,4.5C20.74,3.8 20.2,3.26 19.5,3.08Z" />
				--></svg>
				Imprimir
			</label>
			<input class="toolbar-button-input" type="radio" name="collage-tools" id="collage-themes">
			<label class="toolbar-button"  onclick="buy_puntos()" >
				<svg class="toolbar-icon" viewBox="0 0 24 24">
					<path fill="currentColor" d="M21.47,4.35L20.13,3.79V12.82L22.56,6.96C22.97,5.94 22.5,4.77 21.47,4.35M1.97,8.05L6.93,20C7.24,20.77 7.97,21.24 8.74,21.26C9,21.26 9.27,21.21 9.53,21.1L16.9,18.05C17.65,17.74 18.11,17 18.13,16.26C18.14,16 18.09,15.71 18,15.45L13,3.5C12.71,2.73 11.97,2.26 11.19,2.25C10.93,2.25 10.67,2.31 10.42,2.4L3.06,5.45C2.04,5.87 1.55,7.04 1.97,8.05M18.12,4.25A2,2 0 0,0 16.12,2.25H14.67L18.12,10.59" />
				</svg>
				Comprar Puntos
			</label>
			<!-- Meine Bilder -->
			<div class="toolbar-content toolbar-content--has-tabs" id="collage-images-content">
				<input class="toolbar-button-input" id="collage-images-innerTab--images" type="radio" name="collage-images" checked>
				<label for="collage-images-innerTab--images" class="toolbar-button-inner">Imagenes Prediseñadas</label>

				<input class="toolbar-button-input" id="collage-images-innerTab--collages" type="radio" name="collage-images">
				<label for="collage-images-innerTab--collages" class="toolbar-button-inner">Imagenes de Usuario</label>

				<div id="collage-images-innerContent--collages" class="toolbar-contenttab">
					<div class="toolbar-content__section">
						<p class="toolbar-content__section-headline">Imagenes del usuario</p>
						<div class="toolbar-content__grid toolbar-content__grid--1-1" id="user_image_container">
							<div id="drop-region" class="toolbar-content__grid-item ddzone" alt="neue Collage" title="neue Collage" unload_zone="user_image_container" unload_action="add_to_pool">
								<button class="toolbar-content__grid-button">
									<svg style="width:24px;height:24px" viewBox="0 0 24 24">
										<path fill="currentColor" d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z" />
									</svg>
								</button>
							</div>
						</div>
					</div>
				</div>

				<div id="collage-images-innerContent--images" class="toolbar-contenttab">
					<div class="toolbar-content__section">
						<p class="toolbar-content__section-headline">Imagenes prediseñadas</p>
						<div class="toolbar-content__grid toolbar-content__grid--1-1" id="premade_images_container">
							<div class="toolbar-content__grid-item">
								<img src="assets/img/editor_preset_img/default-1.jpeg" 
								ondragstart="OnStartDragSelectableImage(event)"  draggable="true"
								alt="" class="toolbar-content__grid-image selectable_image" />
							</div>
							<div class="toolbar-content__grid-item">
								<img src="assets/img/editor_preset_img/default-2.jpeg" 
								ondragstart="OnStartDragSelectableImage(event)"  draggable="true"
								alt="" class="toolbar-content__grid-image selectable_image" />
							</div>
							<div class="toolbar-content__grid-item">
								<img src="assets/img/editor_preset_img/default-3.jpeg" 
								ondragstart="OnStartDragSelectableImage(event)"  draggable="true"
								alt="" class="toolbar-content__grid-image selectable_image" />
							</div>
							<div class="toolbar-content__grid-item">
								<img src="assets/img/editor_preset_img/default-4.jpeg" 
								ondragstart="OnStartDragSelectableImage(event)"  draggable="true"
								alt="" class="toolbar-content__grid-image selectable_image" />
							</div>
							<div class="toolbar-content__grid-item">
								<img src="assets/img/editor_preset_img/default-5.jpeg" 
								ondragstart="OnStartDragSelectableImage(event)"  draggable="true"
								alt="" class="toolbar-content__grid-image selectable_image" />
							</div>
							<div class="toolbar-content__grid-item">
								<img src="assets/img/editor_preset_img/default-6.jpeg" 
								ondragstart="OnStartDragSelectableImage(event)"  draggable="true"
								alt="" class="toolbar-content__grid-image selectable_image" />
							</div>
							<div class="toolbar-content__grid-item">
								<img src="assets/img/editor_preset_img/default-7.jpeg" 
								ondragstart="OnStartDragSelectableImage(event)"  draggable="true"
								alt="" class="toolbar-content__grid-image selectable_image" />
							</div>
							<div class="toolbar-content__grid-item">
								<img src="assets/img/editor_preset_img/default-8.jpeg" 
								ondragstart="OnStartDragSelectableImage(event)"  draggable="true"
								alt="" class="toolbar-content__grid-image selectable_image" />
							</div>
							<div class="toolbar-content__grid-item">
								<img src="assets/img/editor_preset_img/default-9.jpeg" 
								ondragstart="OnStartDragSelectableImage(event)"  draggable="true"
								alt="" class="toolbar-content__grid-image selectable_image" />
							</div>
							<div class="toolbar-content__grid-item">
								<img src="assets/img/editor_preset_img/default-10.jpeg" 
								ondragstart="OnStartDragSelectableImage(event)"  draggable="true"
								alt="" class="toolbar-content__grid-image selectable_image" />
							</div>
							<div class="toolbar-content__grid-item">
								<img src="assets/img/editor_preset_img/default-11.jpeg" 
								ondragstart="OnStartDragSelectableImage(event)"  draggable="true"
								alt="" class="toolbar-content__grid-image selectable_image" />
							</div>
							<div class="toolbar-content__grid-item">
								<img src="assets/img/editor_preset_img/default-12.jpeg" 
								ondragstart="OnStartDragSelectableImage(event)"  draggable="true"
								alt="" class="toolbar-content__grid-image selectable_image" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Layout -->
			<div class="toolbar-content" id="collage-layouts-content">
				<div class="toolbar-content__section">
					<p>Plantilla</p>
					<div class="toolbar-content__grid toolbar-content__grid--1-1 toolbar-content__formats toolbar-content__horizontal">
						<div class="toolbar-content__grid-item-new border-color:black" onclick="load_grid_template('template_1')">
							<img src="assets/img/Templates/Template_1.png" alt="" class="toolbar-content__grid-image-new"  onclick="load_grid_template('template_1')"/>
						</div>
						<div class="toolbar-content__grid-item-new" onclick="load_grid_template('template_2')">
							<img src="assets/img/Templates/Template_2.png" alt="" class="toolbar-content__grid-image-new"  onclick="load_grid_template('template_2')"/>
						</div>
						<div class="toolbar-content__grid-item-new" onclick="load_grid_template('template_3')">
							<img src="assets/img/Templates/Template_3.png" alt="" class="toolbar-content__grid-image-new"  onclick="load_grid_template('template_3')"/>
						</div>
						<div class="toolbar-content__grid-item-new" onclick="load_grid_template('template_4')">
							<img src="assets/img/Templates/Template_4.png" alt="" class="toolbar-content__grid-image-new"  onclick="load_grid_template('template_4')"/>
						</div>
						<div class="toolbar-content__grid-item-new" onclick="load_grid_template('template_5')">
							<img src="assets/img/Templates/Template_5.png" alt="" class="toolbar-content__grid-image-new"  onclick="load_grid_template('template_5')"/>
						</div>
						<div class="toolbar-content__grid-item-new" onclick="load_grid_template('template_6')">
							<img src="assets/img/Templates/Template_6.png" alt="" class="toolbar-content__grid-image-new"  onclick="load_grid_template('template_6')"/>
						</div>
						<!--div class="toolbar-content__grid-item-new" onclick="load_grid_template('template_7')">
							<img src="assets/img/Templates/Template_7.png" alt="" class="toolbar-content__grid-image-new"  onclick="load_grid_template('template_7')"/>
						</div>
						<div class="toolbar-content__grid-item-new" onclick="load_grid_template('template_8')">
							<img src="assets/img/Templates/Template_8.png" alt="" class="toolbar-content__grid-image-new"  onclick="load_grid_template('template_8')"/>
						</div-->
						<div class="toolbar-content__grid-item-new">
							<button class="toolbar-content__grid-button" onclick="add_new_panel()">
								<svg style="width:24px;height:24px" viewBox="0 0 24 24">
									<path fill="currentColor" d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z" />
								</svg>
							</button>
						</div>
						<!--div class="toolbar-content__grid-item">
							<div class="toolbar-content__format-holder">
								<div class="toolbar-content__format toolbar-content__format--1-2" onclick="load_grid_template('template_7')">1:2</div>
							</div>
						</div-->
					</div>
				</div>
				<div class="toolbar-content__section">
					<p>Horientación</p>
					<div class="select_mate" data-mate-select="active" style="width:100%" functionId="change_page_align">
						<select name="change_page_align" onclick="return false;" id="page_align">
							<option value="horizontal">Horizontal</option>
							<option value="vertical">Vertical</option>
						</select>
						<p class="selecionado_opcion"  onclick="open_select(this)" ></p><span onclick="open_select(this)" class="icon_select_mate" ><svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
							<path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z"/>
							<path d="M0-.75h24v24H0z" fill="none"/>
						</svg></span>
						<div class="cont_list_select_mate">
							<ul class="cont_select_int">  </ul> 
						</div>
					</div>


				</div>
			</div>
			<!-- Text -->
			<div class="toolbar-content toolbar-content--has-tabs" id="collage-text-content">
				<input class="toolbar-button-input" id="collage-texts-innerTab--signatures" type="radio" name="collage-texts" checked>
				<label for="collage-texts-innerTab--signatures" class="toolbar-button-inner">Firmas</label>

				<input class="toolbar-button-input" id="collage-texts-innerTab--preview" type="radio" name="collage-texts">
				<label for="collage-texts-innerTab--preview" class="toolbar-button-inner">Crear firma</label>

				<div id="collage-text-innerContent--signatures" class="toolbar-contenttab">
					<div class="toolbar-content__section">
						<p>Firma</p>
						<div class="toolbar-content__grid toolbar-content__grid--1-1" id="user_signature">
						</div>
						<div>
							<div class="row" style="margin:5px !important;">
								<button class="btn_select_signature_for_collage btn btn-success sm-6" style="margin:5px !important;" onclick="select_signature('signature_preview_container','set_signature')">Usar</button>
								<button class="btn_select_signature_for_collage btn btn-danger" style="margin:5px !important;" onclick="select_signature('signature_preview_container','remove_signature')">Remover</button>
								<button class=" btn btn-primary" style="margin:5px !important;" onclick="show_modal_crear()">Crear</button>
							</div>
						</div>
					</div>
				</div>
				<div id="collage-text-innerContent--preview" class="toolbar-contenttab">
					<div class="toolbar-content__section">
						
						<br>
						<div class="footer_signature_config_section">
							<p>Configuracion de la firma</p>
							<div>
								<table class="toolbar-content__margins">
									<tr>
										<td>
											<p class="toolbar-content__margins-label">Ancho<span class="toolbar-content__margins-value" id="signature_width_value_collage">20</span></p>
										</td>
									</tr>
									<tr>
										<td>
											<div class="margin-slider">
												<input type="range" id="signature_width_scroll_collage" min="20" max="60" value="20" class="margin-slider-input" onchange="signature_width_change('collage')">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<p class="toolbar-content__margins-label">Posicion de la firma<span class="toolbar-content__margins-value" id="signature_position_value" >0</span></p>
										</td>
									</tr>
									<tr>
										<td>
											<div class="margin-slider">
												<input type="range" id="signature_position_scroll" min="0" max="100" value="0" class="margin-slider-input" onchange="signature_position_change()">
											</div>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Hintergrund -->
			<div class="toolbar-content toolbar-content--has-tabs" id="collage-background-content">
				<input class="toolbar-button-input" id="collage-backgrounds-innerTab--colors" type="radio" name="collage-backgrounds" checked>
				<label for="collage-backgrounds-innerTab--colors" class="toolbar-button-inner">Fondo</label>

				<input class="toolbar-button-input" id="collage-backgrounds-innerTab--borders" type="radio" name="collage-backgrounds">
				<label for="collage-backgrounds-innerTab--borders" class="toolbar-button-inner">Borde</label>
				<!-- Farben -->
				<div id="collage-background-innerContent--colors" class="toolbar-contenttab">
					<div class="toolbar-content__section">
						<p>Color de fondo</p>
						<div class="toolbar-content__horizontal toolbar-content__grid toolbar-content__grid--1-1 toolbar-content__grid--colors">
							
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#FFFFFF" onclick="change_collage_background_color('#FFFFFF')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#000000" onclick="change_collage_background_color('#000000')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#eaeaea" onclick="change_collage_background_color('#eaeaea')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button color--active" style="background:#7fffd6" onclick="change_collage_background_color('#7fffd6')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#ffea83" onclick="change_collage_background_color('#ffea83')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#e54661" onclick="change_collage_background_color('#e54661')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#ffa67f" onclick="change_collage_background_color('#ffa67f')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#5fbdf3" onclick="change_collage_background_color('#5fbdf3')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#fbc1ff" onclick="change_collage_background_color('#fbc1ff')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#468966" onclick="change_collage_background_color('#468966')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#f6b604" onclick="change_collage_background_color('#f6b604')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#b64926" onclick="change_collage_background_color('#b64926')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#225378" onclick="change_collage_background_color('#225378')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#9cf281" onclick="change_collage_background_color('#9cf281')"></button>
							</div>
						</div>
						
					</div>
				</div>
				<!-- Ränder -->
				<div id="collage-background-innerContent--borders" class="toolbar-contenttab">
					<div class="toolbar-content__section">
						<p>Color de borde</p>
						<div class="toolbar-content__horizontal toolbar-content__grid toolbar-content__grid--1-1 toolbar-content__grid--colors">
							
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#FFFFFF" onclick="change_collage_border_color('#FFFFFF')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#000000" onclick="change_collage_border_color('#000000')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#eaeaea" onclick="change_collage_border_color('#eaeaea')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button color--active" style="background:#7fffd6" onclick="change_collage_border_color('#7fffd6')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#ffea83" onclick="change_collage_border_color('#ffea83')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#e54661" onclick="change_collage_border_color('#e54661')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#ffa67f" onclick="change_collage_border_color('#ffa67f')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#5fbdf3" onclick="change_collage_border_color('#5fbdf3')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#fbc1ff" onclick="change_collage_border_color('#fbc1ff')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#468966" onclick="change_collage_border_color('#468966')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#f6b604" onclick="change_collage_border_color('#f6b604')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#b64926" onclick="change_collage_border_color('#b64926')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#225378" onclick="change_collage_border_color('#225378')"></button>
							</div>
							<div class="toolbar-content__grid-item">
								<button class="toolbar-content__grid-button" style="background:#9cf281" onclick="change_collage_border_color('#9cf281')"></button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Vorlagen -->
			<div class="toolbar-content" id="collage-themes-content">
				<div class="toolbar-content__section">
					<p>Opciones de Exportación</p>
					<div></div>
					<button onclick="print_collage()" style="width:100%;"  class="btn btn-secondary">Imprimir</button>
					<div><br></div>
					<p>Opciones de Cuenta</p>					
					<button onclick="buy_puntos()" id="btnComprarSide" style="width:100%;"  class="btn btn-secondary">Comprar Puntos</button>
					<!--p>PDF</p>					
					<button onclick="make_pdf()" id="btnComprarSide" style="width:100%;"  class="btn btn-secondary">Comprar PDF</button-->
				</div>
			</div>
		</div>
		<div class="collage-content" style="height: fit-content !important;" id="collage_content_area">
			<!--<div class="collage-content-tools">Tools</div>-->
			<div class="collage-content-canvas" id="collage_print_zone" style="height: fit-content !important;" >
				<div id="canvas" class="grid-stack">
					<div class="grid-stack-item">
						<div class="grid-stack-item-content"></div>
					</div>
					<div class="grid-stack-item" data-gs-width="2">
						<div class="grid-stack-item-content"></div>
					</div>
				</div>
				<div id="signature_collage_zone" class="signature_collage_zone" >
				</div>
			</div>
		</div>
		<iframe id="printf" name="printf" onload="print_collage_iframe()" style="display:none;"></iframe>
	</div>
	<!--<div class="collage-footer"><button class="button button-cta">Weiter</button></div>			-->
</div>

<!--Begin Modal Login-->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header border-bottom-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-title text-center">
          <h4>Ingreso</h4>
        </div>
        <div class="d-flex flex-column text-center">
          <form>
            <div class="form-group">
              <input type="email" class="form-control" id="txtCorreoL"placeholder="Correo">
            </div>
            <div class="form-group">
              <input type="password" class="form-control" id="txtClaveL" placeholder="Contraseña">
            </div>
            <button type="button" class="btn btn-success btn-block btn-round" onclick="login()">Ingresar</button>
          </form>
          
          <div class="text-center text-muted delimiter"><!--or use a social network--></div>
            
        </div>
      </div>
      
      <div class="modal-footer d-flex justify-content-center">
        <div class="signup-section">¿Aun no te has registrado? <a href="#" onclick="RegistrarUsuario()" class="text-info"> Registrate</a>.</div>
      </div>
    </div>
  </div>
</div>
<!--End Modal Login-->

<!-- Begin Modal Register-->
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered " role="document">
    <div class="modal-content">
      <div class="modal-header border-bottom-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-title text-center">
          <h4>Registro</h4>
        </div>
        <div class="d-flex flex-column text-center">
            <form>
                <div class="form-group">
                    <input type="text" class="form-control" id="txtDocumento" maxlength="8" onkeypress='return event.charCode >= 48 && event.charCode <= 57' placeholder="Nro. Dni">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="txtNombres"placeholder="Nombre Completo">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="txtCmp"placeholder="CMP" maxlength="6" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="txtCiudad"placeholder="Ciudad">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="txtModelo"placeholder="Modelo de Ecógrafo">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="txtMarca"placeholder="Marca de Ecógrafo">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="txtCorreo" placeholder="Correo">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="txtClave" placeholder="Contraseña">
                </div>                
                <div class="form-group">
                    <input type="password" class="form-control" id="txtClaveReingreso" placeholder="Repita Contraseña">
                </div>                              
                <div class="form-group">
                    <input type="text" class="form-control" id="txtTelefono" placeholder="Telefono(Opcional)*" maxlength="9" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                </div>
                <button type="button" class="btn btn-success btn-block btn-round" onclick="registrar()">Registrar</button>
            </form>
          
          <div class="text-center text-muted delimiter"><!--or use a social network--></div>
          <div class="d-flex justify-content-center social-buttons">
            <!--<button type="button" class="btn btn-secondary btn-round" data-toggle="tooltip" data-placement="top" title="Twitter">
              <i class="fab fa-twitter"></i>
            </button>
            <button type="button" class="btn btn-secondary btn-round" data-toggle="tooltip" data-placement="top" title="Facebook">
              <i class="fab fa-facebook"></i>
            </button>
            <button type="button" class="btn btn-secondary btn-round" data-toggle="tooltip" data-placement="top" title="Linkedin">
              <i class="fab fa-linkedin"></i>
            </button>-->
          </div>
        </div>
      </div>
      
      <div class="modal-footer d-flex justify-content-center">
        <div class="signup-section"><!--¿Aun no te has registrado? <a href="#" onnclick="RegistrarUsuario()" class="text-info"> Registrate</a>.--></div>
      </div>
    </div>
  </div>
</div>
<!--End Modal Register-->
<!-- Modal Buy Section Begin-->
<div class="modal fade bd-example-modal-lg" id="buyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg set-bg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Información de Pago</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="container-card"> 
  <div class="col1">
    <div class="cardpay">
      <div class="front">
        <div class="type">
          <img class="bankid"/>
        </div>
        <span class="chip"></span>
        <span class="card_number">&#x25CF;&#x25CF;&#x25CF;&#x25CF; &#x25CF;&#x25CF;&#x25CF;&#x25CF; &#x25CF;&#x25CF;&#x25CF;&#x25CF; &#x25CF;&#x25CF;&#x25CF;&#x25CF; </span>
        <div class="date"><span class="date_value">MM / YYYY</span></div>
        <span class="fullname">FULL NAME</span>
      </div>
      <div class="back">
        <div class="magnetic"></div>
        <div class="bar"></div>
        <span class="seccode">&#x25CF;&#x25CF;&#x25CF;</span>
        <span class="chip"></span><span class="disclaimer">This card is property of Random Bank of Random corporation. <br> If found please return to Random Bank of Random corporation - 21968 Paris, Verdi Street, 34 </span>
      </div>
    </div>
  </div>
  <div class="col2">
    <label>Documento</label>
    <input class=""  id="documentoCard" type="text" ng-model="dni" maxlength="8" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
    <label>Titular de la tarjeta</label>
    <input class="inputname" id="titularCard" type="text" placeholder=""/>
    <label>Numero de la tarjeta</label>
    <input class="number" id="numberCard" type="text" ng-model="ncard" maxlength="19" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
    <label>Fecha de vencimiento</label>
    <input class="expire" id="dateCard" type="text" placeholder="MM / YYYY"/>
    <label>CVC</label>
    <input class="ccv" type="text" id="ccvCard" placeholder="CVC" maxlength="3" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
    <label>Monto</label>
    <input class="" id="Monto" type="text" placeholder="Monto" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
	
    <label>Puntos</label>
    <input class="" id="Puntos" type="text" placeholder="Puntos" onkeypress='return event.charCode >= 48 && event.charCode <= 57' disabled/>
    <button class="buy" onclick="registrarVenta()"><!--<i class="material-icons">lock</i> -->Comprar</button>
  </div>
</div>
      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>-->
    </div>
  </div>
</div>
<!-- Modal Buy Section End-->

<!-- Modal Firma Section Begin-->
<div class="modal fade bd-example-modal-lg" id="firmaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg set-bg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Información de Pago</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<p>Fuente</p>
						<div class="toolbar-content__fonts">
							<div class="select_mate" data-mate-select="active" style="width:100%" functionId="change_page_align">
								<select name="select_font" onclick="return false;" id="">
									<option value="font-family: Arial">Arial</option>
									<option value="font-family: ´Times New Roman">Times New Roman</option>
									<option value="font-family: Georgia" >Georgia</option>
									<option value="font-family: Consolas">Consolas</option>
								</select>
								<p class="selecionado_opcion"  onclick="open_select(this)" ></p><span onclick="open_select(this)" class="icon_select_mate" ><svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
									<path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z"/>
									<path d="M0-.75h24v24H0z" fill="none"/>
								</svg></span>
								<div class="cont_list_select_mate">
									<ul class="cont_select_int">  </ul> 
								</div>
							</div>
						</div>
						<p style="margin-top:20px">Dedicatoria</p>
						<div class="toolbar-content__horizontal">
							<div class="row" style="width:97%;margin-left:0!important;">
							<textarea class="col-12" id="t_dedicatoria" name="t_dedicatoria" rows="4" style="width:97%;" placeholder="Firma / Dedicatoria" onchange="reload_preview()"></textarea></div>
						</div>
						<div>
							<p>Configuracion del preview</p>
							
							<div>
								<table class="toolbar-content__margins">
									<tr marker="preview_signature_config_section">
										<td style="width:100%;">
											<p class="toolbar-content__margins-label">Ancho de firma<span class="toolbar-content__margins-value" id="signature_width_value">60</span></p>
										</td>
									</tr>
									<tr marker="preview_signature_config_section">
										<td style="width: 100%;" >
											<div class="margin-slider" style="width:100%;">
												<input type="range" id="signature_width_scroll" min="100" max="1000" value="60" class="margin-slider-input" onchange="signature_width_change('preview')">
											</div>
										</td>
									</tr>
									<tr  marker="preview_signature_config_section">
										<td style="width: 100%;">
											<p class="toolbar-content__margins-label">Porcentaje de firma<span class="toolbar-content__margins-value" id="signature_percentage_value">100</span></p>
										</td>
									</tr>
									<tr marker="preview_signature_config_section">
										<td style="width: 100%;" >
											<div class="margin-slider" style="width:100%;">
												<input type="range" id="signature_percentage_scroll" min="20" max="100" value="100" class="margin-slider-input" onchange="signature_distribution_change('preview')">
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<p class="toolbar-content__margins-label">Imagen</p>
										</td>
									</tr>
									<tr>
										<td>
											<div class="margin-slider">
												<div id="user_signature_drop_region" class="toolbar-content__grid-item ddzone" alt="Firma" title="Firma" unload_zone="signature_img" unload_action="set_as_src">
													<button class="toolbar-content__grid-button">
														<svg style="width:24px;height:24px" viewBox="0 0 24 24">
															<path fill="currentColor" d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z" />
														</svg>
													</button>
												</div>
											</div>
										</td>
									</tr>
								</table>
							</div>
						</div>
						<p>Previsualización</p>
						<div class="editor_preview_wrapper" id="signature_preview_wrapper">
							<div class="editor_preview_container" id="signature_preview_container">
								<div id="text_preview" class="editor_text_preview"></div>
								<img id="signature_img" class="editor_signature_img" src="" alt=" "></img>
							</div>
						</div>
						<div class="editor_preview_wrapper" id="signature_preview_wrapper2" style="display:none;">
							<div class="editor_preview_container">
								<object  id="signature_preview_img" src=""></object >
							</div>
						</div>
						<!--<div>
							<div class="row" style="margin:5px !important;">
								<button  class="preview_signature_config_section btn btn-primary" style="margin:5px !important;" onclick="save_signature('signature_preview_container','b64')">Guardar</button>
							</div>
						</div>-->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="reset_signature_preview()">Cerrar</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="save_signature('signature_preview_container','b64')">Guardar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Firma Section End-->

<!--Begin Modal Editor Image-->
<div class="modal fade bd-example-modal-lg" id="editorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-dialog-centered modal-lg set-bg" role="document" style=" width:60vw !important;">
		<div class="modal-content" style="max-height:80vh;">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Editar Imagen</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="container-card" style="width:100% !important;"> 
					<div class="col1" id="editor">
						<img id="editImage" src="" alt="">
					</div>
					<div class="col2">
						<!--<div class="row">	
					 		<label>Opciones</label>						
						</div>
						<div class="row">	
					 		<button id="image" type="button" style="" class="btn btn-primary" value="Recortar">Recortar</button>					
						</div>-->
						<!--div class="row">	
					 		<div class="margin-slider" >
								<label>Bordes</label>	
								<input type="range" id="rngBordes" min="0" max="50" value="0" class="margin-slider-input" onchange="borderRadious()">
							</div>					
						</div-->
						<div class="row">	
					 		<label>Previsualización</label>				
						</div>
						<div id="content__Preview">

							<div class="row" id="preview__content" >	
								<canvas id="preview" ></canvas>		
							</div>
						</div>
						
					</div>
				</div>
			</div>
		
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary"  data-dismiss="modal" >Cerrar</button>
				<button type="button" class="btn btn-primary" onclick="saveEditImage();" data-dismiss="modal" >Guardar</button>
			</div>
		</div>
  	</div>
</div>
<!--End Modal Editor Image-->
<!--Steps Modal Login-->
<div class="modal fade bd-example-modal-lg" id="stepsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header border-bottom-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <section class="services spad">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="section-title">
							<span>En Cuatro Sencillos Pasos</span>
							<h2>Cómo Hacer un Collage</h2>
						</div>
					</div>
				</div>
				<div class="row" id="steps">
				</div>
			</div>
		</section>
      <div class="modal-footer d-flex justify-content-center">
        <button type="button" class="btn btn-secondary"  data-dismiss="modal" >Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!--End Modal Steps-->


<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/gridstack/gridstack.all.js"></script>
<script type="text/javascript" src="js/jsPDF/polyfills.umd.js"></script>
<script type="text/javascript" src="js/jsPDF/jspdf.umd.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="css/cso/js/index.js"></script>
<script src="js/dom-to-image.min.js"></script>
<script src="js/editor_templates.js"></script>
<script src="js/editor_preloader.js"></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script src="//cdn.jsdelivr.net/gh/mar10/jquery-ui-contextmenu@v1/jquery.ui-contextmenu.min.js"></script>

    <script src="js/cropper.js"></script>
<script src="js/editor.js"></script>
<script src="js/navbar-js.js"></script>
<script src="js/jscardpay.js"></script>
</body>

</html>