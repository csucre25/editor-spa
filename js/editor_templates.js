class Grid_Template_Master {
    
    constructor() {
      this.template_list = [
            {
                column:10,
                widgets:[{"x":0,"y":0,"width":6,"height":"page-height_50"},{"x":6,"y":0,"width":6,"height":"page-height_50"},{"x":0,"y":"fit_0","width":6,"height":"page-height_50"},{"x":6,"y":"fit_1","width":6,"height":"page-height_50"}],
                template_name:"Plantilla 1",
                template_id:"template_1"
            },
            {
                column:12,
                widgets:[{"x":0,"y":0,"width":4,"height":"page-height_33"},{"x":0,"y":"fit_0","width":4,"height":"page-height_33"},{"x":0,"y":"fit_1","width":4,"height":"page-height_32"}],
                template_name:"Plantilla 2",
                template_id:"template_2"
            },
            {
                column:12,
                widgets:[{"x":0,"y":0,"width":4,"height":"page-height_33"},{"x":4,"y":0,"width":4,"height":"page-height_33"},{"x":8,"y":0,"width":4,"height":"page-height_33"},
                {"x":0,"y":"fit_0","width":4,"height":"page-height_33"},{"x":4,"y":"fit_1","width":4,"height":"page-height_33"},{"x":8,"y":"fit_2","width":4,"height":"page-height_33"},
                {"x":0,"y":"fit_3","width":4,"height":"page-height_32"},{"x":4,"y":"fit_4","width":4,"height":"page-height_32"},{"x":8,"y":"fit_5","width":4,"height":"page-height_32"}],
                template_name:"Plantilla 3",
                template_id:"template_3"
            },
            {
                column:12,
                widgets:[{"x":0,"y":0,"width":4,"height":"page-height_50"},{"x":4,"y":0,"width":4,"height":"page-height_50"},{"x":8,"y":0,"width":4,"height":"page-height_50"}],
                template_name:"Plantilla 4",
                template_id:"template_4"
            },
            {
                column:12,
                widgets:[{"x":0,"y":0,"width":6,"height":"page-height_50"},{"x":6,"y":0,"width":6,"height":"page-height_50"}],
                template_name:"Plantilla 5",
                template_id:"template_5"
            },
            {
                column:12,
                widgets:[{"x":0,"y":0,"width":4,"height":"page-height_33"},{"x":4,"y":0,"width":4,"height":"page-height_33"},{"x":8,"y":0,"width":4,"height":"page-height_33"},
                {"x":0,"y":"fit_0","width":4,"height":"page-height_33"},{"x":4,"y":"fit_1","width":4,"height":"page-height_33"},{"x":8,"y":"fit_2","width":4,"height":"page-height_33"}],
                template_name:"Plantilla 6",
                template_id:"template_6"
            },
            {
                column:12,
                widgets:[{"x":0,"y":0,"width":12,"height":5},{"x":0,"y":5,"width":4,"height":1},{"x":4,"y":5,"width":4,"height":1},{"x":8,"y":5,"width":4,"height":1}],
                template_name:"Plantilla 7",
                template_id:"template_7"
            },
            {
                column:12,
                widgets:[{"x":2,"y":0,"width":6,"height":6},{"x":8,"y":0,"width":2,"height":2},{"x":8,"y":2,"width":2,"height":2},{"x":8,"y":4,"width":2,"height":2}],
                template_name:"Plantilla 8",
                template_id:"template_8"
            }
            
        
        ];
    }

    get_template_by_id(template_id,get_default=false){
        if (get_default){
            return this.template_list[0]
        }
        let total_elements = this.template_list.length
        for (var iterator=0;iterator < total_elements;iterator = iterator+1 ){
            if (this.template_list[iterator].template_id == template_id){
                return this.template_list[iterator]
            }
        }
        return false
    }
    get_total_grid_rows(grid){
        return collage_data.height / 10
    }
    load_template(grid,template){
        const find_command = (value,term) => {
            if (value.includes(term)){
                return value;
            }
        };


        var templates_copy = JSON.parse(JSON.stringify(template));
        grid.removeAll(true);
        var proportions = {
            x : parseFloat(collage_zone_data.collage.style.width.replace("px","")) / 12,
            total_y :parseFloat(collage_zone_data.collage.style.height.replace("px","")),
            y : parseFloat(collage_zone_data.collage.style.height.replace("px","")) / 10,
        }
        var total_rows = parseFloat(collage_zone_data.collage.style.height.replace("px","")) / 10
        //grid.column(template.column,false)
        let total_widgets = templates_copy.widgets.length
        
        for (var iterator=0;iterator < total_widgets;iterator = iterator+1 ){
            let percentage,estimated_height;
            if (typeof templates_copy.widgets[iterator].height === 'string'){
                let command = templates_copy.widgets[iterator].height
                switch (templates_copy.widgets[iterator].height){
                    case find_command(command,"square"): 
                        estimated_height = (proportions.x / 10) * templates_copy.widgets[iterator].width
                        templates_copy.widgets[iterator].height = Math.round(estimated_height)
                        break;
                    case find_command(command,"page-height"):
                        percentage = command.split("_")[1]
                        estimated_height = (proportions.total_y * (percentage/100)) / 10
                        templates_copy.widgets[iterator].height = Math.round(estimated_height)
                        break;
                }
            }
            
            if (typeof templates_copy.widgets[iterator].y == "string"){
                let y_params = templates_copy.widgets[iterator].y.split("_")
                switch (y_params[0]){
                    case "fit": 
                    let element_to_fit = templates_copy.widgets[parseInt(y_params[1])]
                    let new_y = element_to_fit.y + element_to_fit.height
                    templates_copy.widgets[iterator].y = new_y
                    break;
                }
            }
            
            grid.addWidget('<div><div class="grid-stack-item-content"></div></div>', {
                width: templates_copy.widgets[iterator].width,
                height: templates_copy.widgets[iterator].height,
                x: templates_copy.widgets[iterator].x,
                y: templates_copy.widgets[iterator].y,
            });
        }
    }
  }


var template_master = new Grid_Template_Master()