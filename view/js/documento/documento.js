//var a espanish datatable 
$("#datatable-Documento").DataTable({
    //dom: 'lfrtip',
    language: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    "lengthMenu": [10, 25, 50, 75, 100]
});

//VARIABLES 
var cboMarca = document.getElementById('cboMarca');
var txtDescripcion = document.getElementById('txtDescripcion');
var txtStock = document.getElementById('txtStock');
var txtUnidadMedida = document.getElementById('txtUnidadMedida');
var txtPrecioVenta = document.getElementById('txtPrecioVenta');
var txtPrecioCosto = document.getElementById('txtPrecioCosto');
var btnGuardar = document.getElementById('btnRegistrar');
var btnCancelar = document.getElementById('btnCancelar');
var btnEditar = document.getElementById('btnEditar');

var cboRazon = document.getElementById('cboRazon');
var cboTipoDoc = document.getElementById('cboTipoDoc');
var txtFechaEmision = document.getElementById('txtFechaEmision');
var txtSerie = document.getElementById('txtSerie');
var txtNumero = document.getElementById('txtNumero');
var txtValorVenta = document.getElementById('txtValorVenta');
var txtIgv = document.getElementById('txtIgv');
var txtTotal = document.getElementById('txtTotal');
var idDocumento;

//funciones



function obtenerID(idDoc, idClien, idTipoPa, fech, ser, num, mon) {
    $('.select2').data('select2').destroy();
    idDocumento = idDoc;
    cboRazon.value = idClien;
    cboTipoDoc.value = idTipoPa;

    cboRazon.disabled = true;
    cboTipoDoc.disabled = true;
    txtFechaEmision.disabled = true;
    txtSerie.disabled = true;
    txtNumero.disabled = true;

    $('.select2').select2();
    txtFechaEmision.value = fech;
    txtSerie.value = ser;
    txtNumero.value = num;
    txtTotal.value = mon;
    txtValorVenta.disabled = true;
    txtIgv.disabled = true;
    btnEditar.style.display = 'inline-block';
    btnCancelar.style.display = 'inline-block';
    btnGuardar.style.display = 'none';
    $('.nav-tabs > .active').next('li').find('a').trigger('click');
}

function listarDocumentos() {

    var param_opcion = 'listDocumento';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/documento/cDocumento.php',
        success: function(data) {
            var table = $("#datatable-Documento").DataTable();
            table.clear().draw();
            var obj = JSON.parse(data);
            var cont = 0;
            console.log(obj);
            obj.forEach(function(e) {
                cont++;
                array = [];
                array.push(e.RazonSocial);
                array.push(e.TipoPago);

                if (e.Estado == '0'){
                    array.push('Pendiente');}
                else if(e.Estado =='1'){
                    array.push('Canjeado');
                }else if(e.Estado =='2'){
                    array.push('Anulado');
                }
                
                array.push(e.Fecha);
                array.push(e.Serie);
                array.push(e.Numero);
                array.push("<button class='btn btn-xs btn-success' onclick='obtenerID(" + e.idDoc + "," + e.idCliente + "," + e.idTipoPago + "," + '"' + e.Fecha + '"' + "," + '"' + e.Serie + '"' + "," + '"' + e.Numero + '"' + "," + '"' + e.Monto + '"' + ")'>Editar</button><button href='#modalEliminar' data-toggle='modal' class='btn btn-xs btn-danger' onclick='eliminar(" + e.idDocumento + ")'>Eliminar</button>");
                table.row.add(array).draw().node();
            });

        },
        error: function(data) {
            alert('Error al mostrar');
        }
    });

}

function guardar() {

    var opcion = 'insertDocumento';
    var Serie = txtSerie.value;
    var Numero = txtNumero.value;
    var Fecha = txtFechaEmision.value;
    var idTipoPago = cboTipoDoc.value;
    var idCliente = cboRazon.value;
    var Monto = txtTotal.value;
    if (Monto <= 0) {
        alert('El monto total debe ser superior a 0');
        return;
    }
    if (Serie != "" & idTipoPago != '0' & Fecha != '' & Numero != "" &
        idCliente != '0' & Monto > 0) {
        $("#btnRegistrar").attr("disabled");
        $.ajax({
            type: 'POST',
            data: 'opcion=' + opcion +
                '&Serie=' + Serie +
                '&Numero=' + Numero +
                '&Fecha=' + Fecha +
                '&idTipoPago=' + idTipoPago +
                '&idCliente=' + idCliente +
                '&Monto=' + Monto,
            url: '../../controlador/documento/cDocumento.php',
            success: function(data) {
                $("#btnRegistrar").removeAttr("disabled");
                var obj = JSON.parse(data);
                listarDocumentos();
                if (obj == 1) {
                    alert('Documento registrado con exito.');
                    llenarcboTipoDocumento();
                    llenarcboClientes();
                    idDocumento = 0;
                    txtSerie.value = "";
                    txtNumero.value = "";
                    txtFechaEmision.value = "";
                    cboTipoDoc.value = 0;
                    cboRazon.value = 0;
                    txtTotal.value = "";
                    txtValorVenta.value = "";
                    txtIgv.value = "";
                    $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                    //$('.select2').select2();
                } else if (obj == 0) {
                    alert('Documento ya se encuentra regitrado.');
                }
            },
            error: function(data) {
                alert('Error al registrar');
                $("#btnRegistrar").removeAttr("disabled");
            }
        });
    } else {
        alert('Llenar campos');

    }

}

function editar() {

    if (confirm("No se permiten cambios en documento, contacte con el administrador")) {
        cancelar();
    }

    /*var opcion = 'updateDocumento';

    
    if (usuari!="" & carg!='0'){
		$.ajax({
			type: 'POST',
			data: 	'opcion=' + opcion +
                    '&idUsuario=' + idUsuario + 
                    '&Usuario=' + usuari+
                    '&Cargo=' + carg,
            url: '../../controlador/seguridad/cUsuario.php',
			success: function(data){
				var obj=JSON.parse(data);
				listarUsuario();
				if (obj == 1) {
                    alert('Usuario editado con exito.');
                    cancelar();
    
                } else if (obj == 4) {
                    alert('Usuario ya existe.');
                }else{
                    alert('No se realizaron modificaciones.');
                }
			},
			error:function(data){
				alert('Error al editar');
			}
		});}else{
            alert('Campos no válidos');
        }*/
}

function eliminar(id) {

    var param_opcion = 'deleteDocumento';
    if (confirm("Seguro de Anular (S/N)??")) {
        $.ajax({
            type: 'POST',
            data: 'opcion=' + param_opcion +
                '&idDocumento=' + id,
            url: '../../controlador/documento/cDocumento.php',
            success: function(data) {
                var obj = JSON.parse(data);
                console.log(data);
                if (obj == 1) {

                    listarDocumentos();

                } else if(obj == 0 ){
                    alert('Documento no se puede eliminar por que fue canjeado');
                }

            },
            error: function(data) {
                alert('Error al eliminar');
            }
        });
    }

}


function cancelar() {
    $('.select2').data('select2').destroy();
    idDocumento = 0;
    cboRazon.value = 0;
    cboTipoDoc.value = 0;

    cboRazon.disabled = false;
    cboTipoDoc.disabled = false;
    txtFechaEmision.disabled = false;
    txtSerie.disabled = false;
    txtNumero.disabled = false;

    $('.select2').select2();
    txtFechaEmision.value = "";
    txtSerie.value = "";
    txtNumero.value = "";
    txtTotal.value = 0;

    txtValorVenta.disabled = false;
    txtIgv.disabled = false;

    btnEditar.style.display = 'none';
    btnCancelar.style.display = 'none';
    btnGuardar.style.display = 'inline-block';
    $('.nav-tabs > .active').prev('li').find('a').trigger('click');


}

function llenarcboTipoDocumento() {

    var param_opcion = 'listTipoDocumento';
    $("#cboTipoDoc").empty();
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/tipodocumento/cTipoDocumento.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(obj);
            $("#cboTipoDoc").html('<option value="0">Seleccionar Tipo Documento...</option>');
            obj.forEach(function(e) {
                $("#cboTipoDoc").append('<option value="' + e.idTipoPago + '">' + e.TipoPago + '</option>');
            });
        },
        error: function(data) {

        }
    });
}

function llenarcboClientes() {

    var param_opcion = 'listCliente';
    $("#cboRazon").empty();
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/cliente/cCliente.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(obj);
            $("#cboRazon").html('<option value="0">Seleccionar Cliente...</option>');
            obj.forEach(function(e) {
                $("#cboRazon").append('<option value="' + e.idCliente + '">' + e.RazonSocial + '</option>');
            });
            $("#cboRazon").select2();
        },
        error: function(data) {

        }
    });
}

function addEventMonto() {
    $("#txtValorVenta").keypress(function(e) {
        if (e.which == 13) {
            txtValorVenta.value = parseFloat(txtValorVenta.value).toFixed(2);
            var MontoIGV = 0;
            MontoIGV =  0.18 * txtValorVenta.value;

            txtIgv.value = parseFloat(MontoIGV).toFixed(2);

            var MontoTotal = parseFloat(txtValorVenta.value) + parseFloat(MontoIGV);
            txtTotal.value = parseFloat(MontoTotal).toFixed(2);
        }
    });
    /*$("#txtIgv").keypress(function(e) {
        if (e.which == 13) {
            txtIgv.value = parseFloat(txtIgv.value).toFixed(2);
            var MontoIGV = 0;
            MontoIGV = txtIgv.value * txtValorVenta.value / 100;
            var MontoTotal = parseFloat(txtValorVenta.value) + parseFloat(MontoIGV);
            txtTotal.value = parseFloat(MontoTotal).toFixed(2);
        }
    });*/
}

function alCargarDocumento() {
    //$('#datatable-Producto').DataTable();
    $(document).ajaxStop($.unblockUI);
    listarDocumentos();
    llenarcboClientes()
    llenarcboTipoDocumento();
    addEventMonto();
    btnEditar.style.display = 'none';
    btnCancelar.style.display = 'none';
    btnCancelar.addEventListener("click", cancelar);
    btnGuardar.addEventListener("click", guardar);
    btnEditar.addEventListener("click", editar);
}
//EVENTOS
window.addEventListener("load", alCargarDocumento);