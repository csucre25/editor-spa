var btnImprimir = document.getElementById('btnImprimir');

var fechaInicial = document.getElementById('fechaInicial');
var fechaFinal = document.getElementById('fechaFinal');

var cboRazon = document.getElementById('cboRazon');


function llenarcboClientes() {

    var opcion = 'listCliente';
    $("#cboRazon").empty();
    $.ajax({
        type: 'POST',
        data: 'opcion=' + opcion,
        url: '../../controlador/cliente/cCliente.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(obj);
            $("#cboRazon").html('<option value="0">Seleccionar Cliente...</option>');
            obj.forEach(function(e) {
                $("#cboRazon").append('<option value="' + e.idCliente + '">' + e.nombres + '</option>');
            });
            $("#cboRazon").select2();
        },
        error: function(data) {

        }
    });
}

function imprimir() {
    var opcion = 'imprimir';
    var idCliente = cboRazon.value;
    var fechaInicio = fechaInicial.value;
    var fechaFin = fechaFinal.value;
    if (idCliente != '0') {
        $.ajax({
            type: 'POST',
            data: 'opcion=' + opcion +
                '&idCliente=' + idCliente +
                '&fechaInicial=' + fechaInicio +
                '&fechaFinal=' + fechaFin,
            url: '../../controlador/reporte/crletra.php',
            success: function(data) {
                var obj = JSON.parse(data);
                console.log(obj);
                if (obj.length > 0) {
                    generaReporte(fechaInicio, fechaFin, obj);
                } else { alert('No hay datos para el reporte'); }


            },
            error: function(data) {
                alert('Error al mostrar');
            }
        });
    } else {
        alert("No selecciono una razon");
    }
}

function generaReporte(fechainicial, fechafinal, objeto) {


    var doc = new jsPDF();
    doc.setFontSize(12);
    doc.text(82, 30, 'ECOTINTA');
    doc.setFontSize(11);
    doc.text(50, 40, 'LISTADO DE VENTAS DEL: ');
    doc.text(102, 40, fechainicial)
    doc.text(125, 40, 'AL');
    doc.text(133, 40, fechafinal)

    doc.setFontSize(9);
    doc.rect(20, 50, 50, 7);
    doc.text(22, 55, 'CLIENTE.');
    doc.rect(70, 50, 15, 7);
    doc.text(72, 55, 'CMP');
    doc.rect(85, 50, 30, 7);
    doc.text(87, 55, 'MODELO ECO.');
    doc.rect(115, 50, 30, 7);
    doc.text(117, 55, 'MARCA ECO.');
    doc.rect(145, 50, 25, 7);
    doc.text(147, 55, 'MONTO');
    doc.rect(170, 50, 26, 7);
    doc.text(172, 55, 'FECHA VENTA');
    //doc.rect(160, 50, 30, 7);
    //doc.text(164, 55, 'FECHA VENCI.');
    var cont = 0;
    var sum = 0.00;
    var resultado = "A";
    objeto.forEach(function(e, index) {
        cont = cont + 7;
        doc.setFontType('normal')
        doc.setFontSize(9);
        doc.rect(20, 50 + cont, 50, 7);
        doc.text(22, 55 + cont, e.nombres.substring(0, 30));
        doc.rect(70, 50 + cont, 15, 7);
        doc.text(72, 55 + cont, e.cmp);
        doc.rect(85, 50 + cont, 30, 7);
        doc.text(87, 55 + cont, e.ecografo_modelo);
        doc.rect(115, 50 + cont, 30, 7);
        doc.text(117, 55 + cont, e.ecografo_marca);
        doc.rect(145, 50 + cont, 25, 7);
        doc.text(147, 55 + cont, e.monto);
        doc.rect(170, 50 + cont, 26, 7);
        doc.text(172, 55 + cont, e.fecha);
        //doc.rect(160, 50 + cont, 30, 7);
        /*doc.text(164, 55+cont,e.fechaVencim);*/

        sum = sum + parseFloat(e.monto);

        if (e == objeto[objeto.length - 1]) {
            resultado = sum.toString();
            cont = cont + 7;
            doc.setFontSize(9);

            doc.setFontType('bold')
            doc.rect(20, 50 + cont, 125, 7);
            //doc.text(22, 55 + cont, e.nombres.substring(0, 30));
            //doc.rect(70, 50 + cont, 75, 7);
            //doc.text(72, 55 + cont, e.cmp);
            //doc.rect(85, 50 + cont, 60, 7);
            doc.text(80, 55 + cont, 'TOTAL');
            doc.rect(145, 50 + cont, 25, 7);
            doc.text(147, 55 + cont, resultado);
            doc.rect(170, 50 + cont, 26, 7);
            doc.text(172, 55 + cont, '------');
            //doc.rect(160, 50 + cont, 30, 7);
            //doc.text(164, 55 + cont, e.cmp);
            sum = 0;
            resultado = "B";
        }

    });

    $("#pdf_preview").attr("src", doc.output('datauristring'));



    /* doc.save ( 'reporte-'+ hoyFecha()+idProducto+'.pdf');*/

}


function hoyFecha() {
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth() + 1;
    var yyyy = hoy.getFullYear();

    dd = addZero(dd);
    mm = addZero(mm);
    var fecha;

    fecha = yyyy + '-' + mm + '-' + dd;


    return fecha;
}

function addZero(i) {
    if (i < 10) {
        i = '0' + i;
    }
    return i;
}



function alCargarDocumento() {
    llenarcboClientes();
    btnImprimir.addEventListener("click", imprimir);
}
//EVENTOS
window.addEventListener("load", alCargarDocumento);