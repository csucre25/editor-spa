

//VARIABLES 
var txtFechaInicio = document.getElementById('txtFechaInicio');
var txtFechaFin = document.getElementById('txtFechaFin');


var pieOptions     = {
    // Boolean - Whether we should show a stroke on each segment
    segmentShowStroke    : true,
    // String - The colour of each segment stroke
    segmentStrokeColor   : '#fff',
    // Number - The width of each segment stroke
    segmentStrokeWidth   : 1,
    // Number - The percentage of the chart that we cut out of the middle
    percentageInnerCutout: 50, // This is 0 for Pie charts
    // Number - Amount of animation steps
    animationSteps       : 100,
    // String - Animation easing effect
    animationEasing      : 'easeOutBounce',
    // Boolean - Whether we animate the rotation of the Doughnut
    animateRotate        : true,
    // Boolean - Whether we animate scaling the Doughnut from the centre
    animateScale         : false,
    // Boolean - whether to make the chart responsive to window resizing
    responsive           : true,
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio  : false,
    // String - A legend template
    legendTemplate       : '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<segments.length; i++){%><li><span style=\'background-color:<%=segments[i].fillColor%>\'></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
    // String - A tooltip template
    tooltipTemplate      : '<%=value %> <%=label%> Doc'
  };



  function handler(e){

    if (txtFechaFin.value !="" & txtFechaInicio.value!=""){
        if (txtFechaFin.value > txtFechaInicio.value || txtFechaFin.value == txtFechaInicio.value){

            var param_opcion = 'obtenCantidadDoc';
          
             // -------------
    // - PIE CHART -
    // -------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#pieChart').get(0).getContext('2d');
    var pieChart       = new Chart(pieChartCanvas);
    var PieData        = [
      {
        value    : 700,
        color    : '#f56954',
        highlight: '#f56954',
        label    : 'Pendiente'
      },
   
      {
        value    : 600,
        color    : '#00c0ef',
        highlight: '#00c0ef',
        label    : 'Canjeado'
      },
    
      {
        value    : 500,
        color    : '#f39c12',
        highlight: '#f39c12',
        label    : 'Anulado'
      }
    ];
    
    // Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions);
    // -----------------
    // - END PIE CHART -
    // -----------------
                $.ajax({
                    type: 'POST',
                    data: 'opcion=' + param_opcion ,
                    url: '../../controlador/panel/cDashBoard.php',
                    success: function(data) {/*
                        var obj = JSON.parse(data);
                        console.log(data);
                        if (obj == 1) {
        
        
                        } else {
                            alert('Error al motrar gráficas');
                        }
        */
                    },
                    error: function(data) {
                        alert('Error al eliminar');
                    }
                });
            

        }else{
            alert("Fecha de inicio es mayor a fecha fin");
        }
    }
  }

function alCargarDocumento(){
  
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1; 
var yyyy = today.getFullYear();

if (dd < 10) { dd = '0' + dd;}
if (mm < 10) { mm = '0' + mm;}

today = yyyy + '-' + mm + '-' + dd;

$('#txtFechaInicio').val(today); $('#txtFechaFin').val(today); 
handler();
 
}
//EVENTOS
window.addEventListener("load", alCargarDocumento);
  