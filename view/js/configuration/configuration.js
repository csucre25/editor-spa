//var a espanish datatable
$("#datatable-Cliente").DataTable({
    //dom: 'lfrtip',
    language: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    "lengthMenu": [10, 25, 50, 75, 100]
});


//VARIABLES 
var txtCantidad = document.getElementById('txtCantidad');
var btnUnlock = document.getElementById('btnUnlock');
var btnEditar = document.getElementById('btnEditar');
var btnCancelar = document.getElementById('btnCancelar');
var FileVideo;
var FileLogo;
var idCliente;



var txtPtosActuales = document.getElementById('txtPtosActuales');
//funciones



function obtenerID(id, puntos) {
    idCliente = id;
    txtPtosActuales.value = puntos;
    //$('.nav-tabs > .active').next('li').find('a').trigger('click');
}

function listarConfig() {

    var param_opcion = 'listConfig';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/configuration/cConfiguration.php',
        success: function(data) {
            var table = $("#datatable-Cliente").DataTable();
            table.clear().draw();
            var obj = JSON.parse(data);
            console.log(obj);
            $('#txtUsuario').val(obj[0].admin);
            $('#txtNombre').val(obj[0].nombre);
            $('#txtTelefono').val(obj[0].telefono);
            $('#txtCorreo').val(obj[0].email);
            $('#txtEmpresa').val(obj[0].empresa);
            $("#imgLogo").attr("src", obj[0].logo);
            $("#imgVideo").attr("src", obj[0].url);
            // $('#txtLogo').val("disabled", "disabled");
            //$('#txtVideo').val("disabled", "disabled");

        },
        error: function(data) {
            alert('Error al mostrar');
        }
    });

}

function editar() {
    var Usuario = $('#txtUsuario').val();
    var Nombre = $('#txtNombre').val();
    var Telefono = $('#txtTelefono').val();
    var Correo = $('#txtCorreo').val();
    var Empresa = $('#txtEmpresa').val();
    var imgLogo = FileLogo;
    var vdVideo = FileVideo;

    var formData = new FormData();
    formData.append("Usuario", Usuario);
    formData.append("Nombre", Nombre);
    formData.append("Telefono", Telefono);
    formData.append("Correo", Correo);
    formData.append("Empresa", Empresa);
    formData.append("Logo", imgLogo);
    formData.append("Video", vdVideo);
    formData.append("opcion", 'updateConfig');
    $.ajax({
        type: 'POST',
        data: formData,
        url: '../../controlador/configuration/cConfiguration.php',
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function() {
            lockFields();
        },
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(data);
            if (obj == 1) {
                listarConfig();
                lockFields();
                alert('Actualizacion exitosa');
            } else {
                alert('Error al actualizar');
            }
        },
        error: function(data) {
            alert('Error al registrar');
            //listarConfig();
            uslockFields();
        }
    });
}

function lockFields() {
    $("#btnUnlock").removeAttr("disabled");
    $('#btnEditar').attr("disabled", "disabled");
    $('#txtUsuario').attr("disabled", "disabled");
    $('#txtNombre').attr("disabled", "disabled");
    $('#txtTelefono').attr("disabled", "disabled");
    $('#txtCorreo').attr("disabled", "disabled");
    $('#txtEmpresa').attr("disabled", "disabled");
    $('#txtLogo').attr("disabled", "disabled");
    $('#txtVideo').attr("disabled", "disabled");
    $("#txtLogo").val('');
    $("#txtVideo").val('');
}

function unlockFields() {
    $('#btnUnlock').attr("disabled", "disabled");
    $("#btnEditar").removeAttr("disabled");
    $("#txtUsuario").removeAttr("disabled");
    $("#txtNombre").removeAttr("disabled");
    $("#txtTelefono").removeAttr("disabled");
    $("#txtCorreo").removeAttr("disabled");
    $("#txtEmpresa").removeAttr("disabled");
    $("#txtLogo").removeAttr("disabled");
    $("#txtVideo").removeAttr("disabled");
}

function cancelar() {
    listarConfig();
    lockFields();
}

function validaArchivos() {

    $("#txtLogo").change(function() {
        var file = this.files[0];
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
            alert('Seleccione un archivo de tipo imagen (JPEG/JPG/PNG).');
            $("#txtLogo").val('');
            return false;
        } else {
            FileLogo = file;
        }
    });

    $("#txtVideo").change(function() {
        var file = this.files[0];
        var imagefile = file.type;
        var match = ["video/mp4"];
        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
            alert('Seleccione un archivo de tipo imagen (MP4).');
            $("#txtVideo").val('');
            return false;
        } else {
            FileVideo = file;
        }
    });
}

function alCargarDocumento() {
    listarConfig();
    lockFields();
    validaArchivos();
    btnUnlock.addEventListener("click", unlockFields);
    btnEditar.addEventListener("click", editar);
    btnCancelar.addEventListener("click", cancelar);
}
//EVENTOS
window.addEventListener("load", alCargarDocumento);