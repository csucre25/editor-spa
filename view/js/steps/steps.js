//var a espanish datatable
$("#datatable-Steps").DataTable({
    //dom: 'lfrtip',
    language: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    "lengthMenu": [10, 25, 50, 75, 100]
});


//VARIABLES 
var txtTitulo = document.getElementById('txtTitulo');
var txtDescripcion = document.getElementById('txtDescripcion');
var txtOrden = document.getElementById('txtOrden');
var txtTituloUp = document.getElementById('txtTituloUp');
var txtDescripcionUp = document.getElementById('txtDescripcionUp');
var txtOrdenUp = document.getElementById('txtOrdenUp');
var btnRegistrar = document.getElementById('btnRegistrar');
var btnCancelar = document.getElementById('btnCancelar');
var btnActualizar = document.getElementById('btnActualizar');
var FileUpd;
var FileReg;
var idStep;



var txtPtosActuales = document.getElementById('txtPtosActuales');
//funciones



function obtenerID(id, titulo, descripcion, url, orden) {
    idStep = id;
    txtTituloUp.value = titulo;
    txtDescripcionUp.value = descripcion;
    txtOrdenUp.value = orden;
    $("#imgActual").attr("src", "../../" + url);
    //$('.nav-tabs > .active').next('li').find('a').trigger('click');
}

function cancelar() {
    txtTitulo.value = "";
    txtDescripcion.value = "";
    txtOrden.value = "";
    $("#txtImagen").val('');
}

function listarSteps() {

    var param_opcion = 'listSteps';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/steps/cSteps.php',
        success: function(data) {
            var table = $("#datatable-Steps").DataTable();
            table.clear().draw();
            var obj = JSON.parse(data);
            var cont = 0;
            console.log(obj);
            obj.forEach(function(e) {
                cont++;
                array = [];
                array.push(e.orden);
                array.push(e.titulo);
                array.push(e.descripcion);
                array.push('<img src="../../' + e.url + '">');
                array.push("<button data-toggle='modal' data-target='#updateModalCenter' class='btn btn-xs btn-warning' onclick='obtenerID(" + e.idstep + "," + '"' + e.titulo + '"' + "," + '"' + e.descripcion + '"' + "," + '"' + e.url + '"' + "," + '"' + e.orden + '"' + ")'>Editar</button><button href='#modalEliminar' data-toggle='modal' class='btn btn-xs btn-danger' onclick='eliminarSteps(" + e.idstep + ")'>Eliminar</button>");
                table.row.add(array).draw().node();
            });

        },
        error: function(data) {
            alert('Error al mostrar');
        }
    });

}

function editar() {

    var Titulo = txtTituloUp.value;
    var Descripcion = txtDescripcionUp.value;
    var Orden = txtOrdenUp.value;
    var file = FileUpd;

    var formData = new FormData();
    formData.append("idStep", idStep);
    formData.append("Titulo", Titulo);
    formData.append("Descripcion", Descripcion);
    formData.append("Orden", Orden);
    formData.append("File", file);
    formData.append("opcion", 'updateSteps');
    $.ajax({
        type: 'POST',
        data: formData,
        url: '../../controlador/steps/cSteps.php',
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function() {
            $('#btnActualizar').attr("disabled", "disabled");
            //$('#fupForm').css("opacity", ".5");
        },
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(data);
            if (obj == 1) {
                listarSteps();
                cancelar();
                alert('Actualización exitosa');
                $("#updateModalCenter").modal("hide");
            } else {
                alert('Error al actualizar');
            }
            //$("#puntosModalCenter").modal("hide");
            $("#btnActualizar").removeAttr("disabled");
        },
        error: function(data) {
            alert('Error al registrar');
        }
    });
}

function registrar() {
    var Titulo = txtTitulo.value;
    var Descripcion = txtDescripcion.value;
    var Orden = txtOrden.value;
    var file = FileReg; //$('#txtImagen').prop('files')[0]; //$("#txtImagen")[0].files

    var formData = new FormData();
    formData.append("Titulo", Titulo);
    formData.append("Descripcion", Descripcion);
    formData.append("Orden", Orden);
    formData.append("File", file);
    formData.append("opcion", 'insertSteps');
    $.ajax({
        type: 'POST',
        data: formData,
        url: '../../controlador/steps/cSteps.php',
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function() {
            $('#btnRegistrar').attr("disabled", "disabled");
            //$('#fupForm').css("opacity", ".5");
        },
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(data);
            if (obj == 1) {
                listarSteps();
                cancelar();
                alert('Registro exitoso');
            } else {
                alert('Error al registrar');
            }
            //$("#puntosModalCenter").modal("hide");
            $("#btnRegistrar").removeAttr("disabled");
        },
        error: function(data) {
            alert('Error al registrar');
        }
    });


}

function eliminarSteps(idStep) {

    var param_opcion = 'deleteSteps';
    if (confirm("Seguro de Anular (S/N)??")) {
        $.ajax({
            type: 'POST',
            data: 'opcion=' + param_opcion +
                '&idStep=' + idStep,
            url: '../../controlador/steps/cSteps.php',
            success: function(data) {
                var obj = JSON.parse(data);
                console.log(data);
                if (obj == 1) {

                    listarSteps();
                    alert('Elemento eliminado');

                } else {
                    alert('Error al eliminar');
                }

            },
            error: function(data) {
                alert('Error al eliminar');
            }
        });
    }

}

function validaImagen() {

    $("#txtImagenUp").change(function() {
        var file = this.files[0];
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
            alert('Seleccione un archivo de tipo imagen (JPEG/JPG/PNG).');
            $("#txtImagenUp").val('');
            return false;
        } else {
            FileUpd = file;
        }
    });

    $("#txtImagen").change(function() {
        var file = this.files[0];
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg"];
        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
            alert('Seleccione un archivo de tipo imagen (JPEG/JPG/PNG).');
            $("#txtImagen").val('');
            return false;
        } else {
            FileReg = file;
        }
    });
}

function alCargarDocumento() {
    listarSteps();
    validaImagen();
    btnRegistrar.addEventListener("click", registrar);
    btnActualizar.addEventListener("click", editar);
    btnCancelar.addEventListener("click", cancelar);
}
//EVENTOS
window.addEventListener("load", alCargarDocumento);