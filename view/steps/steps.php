<?php 
    session_start();  
    if(!isset($_SESSION['S_Cargo']) )
    {
      if($_SESSION['S_Cargo']!='Admin'){
        header("Location:../../index.php");   
      }
    }
    else
    {
      date_default_timezone_set('America/Lima');
    }
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SISTEMA | Mantenedor</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../assets/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../../assets/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../assets/css/AdminLTE.min.css">
  <!-- AdminLTE Skins -->
  <link rel="stylesheet" href="../../assets/css/_all-skins.min.css">
   <!-- DataTables -->
   <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap.min.css">
   <!-- Main style -->
  <link rel="stylesheet" href="../../assets/css/min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="../../assets/css/select2.min.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- top menu -->
     <?php 
      require('../menus/topNavigation.php');
     ?>
    <!-- /top menu -->
  </header>

  <!-- Columna vertical -->
  <aside class="main-sidebar">
    <section class="sidebar">

      <!-- top menu -->
      <?php 
        require('../menus/topMenu.php');
      ?>
      <!-- /top menu -->
      
      <!-- sidebar menu -->
      <?php 
        require('../menus/sideMenu.php');
      ?>
      <!-- /sidebar menu -->
    </section>
  </aside>

  <!-- ***** Contenido de la página *****-->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Landing Page
        <small>Mantenedor</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Mantenedor</a></li>
        <li class="active">Landing Page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
    
<div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Consultar</a></li>
              <li><a href="#tab_2" data-toggle="tab">Nuevo</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
              <table id="datatable-Steps" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Orden</th>
                  <th>Titulo</th>
                  <th>Imagen</th>
                  <th>Descripción</th>
                  <th>Opciones</th>
                </tr>
                </thead>
                <tbody id="bodytable-Steps">
                 
                </tbody>
              </table>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2" >
              <form class="form-horizontal"> 
              <div class="form-group"> 
                <label for="txtTitulo" class="col-sm-3 control-label">Titulo:</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="txtTitulo" placeholder="Titulo">
                  </div>
                </div>
                <div class="form-group">
                  <label for="txtDescripcion" class="col-sm-3 control-label">Titulo:</label>
                  <div class="col-sm-6">
                    <textarea type="textarea" class="form-control" id="txtDescripcion" placeholder="Descripcion" ></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="txtOrden" class="col-sm-3 control-label">Orden:</label>
                  <div class="col-sm-6">
                    <input type="number" class="form-control" id="txtOrden" placeholder="Nro de Orden">
                  </div>
                </div>
                <div class="form-group">
                  <label for="txtImagen" class="col-sm-3 control-label">Seleccione imagen:</label>
                  <div class="col-sm-6 ">
                    <input type="file" id="txtImagen">
                  </div>
                </div>
                <div class="box-footer">
                <button type="button"  class="btn btn-info btn-md" id="btnRegistrar">Registrar</button>
                <button type="button"  class="btn btn-danger btn-md" id="btnCancelar">Cancelar</button>
                </div> 
                 
            </form>
            
            
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
</div>

    </section>
    
    <!-- /.content -->
  </div>
  <!-- ***** Fin del contenido de la página *****-->
<!-- Modal -->
<div class="modal fade" id="updateModalCenter" tabindex="-1" role="dialog" aria-labelledby="updateModalCenter" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Actualizar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form class="form-horizontal"> 
                <div class="form-group">
                  <label for="txtTituloUp" class="col-sm-3 control-label">Titulo:</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="txtTituloUp" placeholder="Titulo">
                  </div>
                </div>
                <br>
                <div class="form-group">
                  <label for="txtDescripcionUp" class="col-sm-3 control-label">Descripcion:</label>
                  <div class="col-sm-6">
                    <textarea type="textarea" class="form-control" id="txtDescripcionUp" placeholder="Descripcion" ></textarea>
                  </div>
                </div>
                <br>
                <div class="form-group">
                  <label for="txtOrdenUp" class="col-sm-3 control-label">Orden:</label>
                  <div class="col-sm-6">
                    <input type="number" class="form-control" id="txtOrdenUp" placeholder="Nro de Orden">
                  </div>
                </div>
                <br>
                <div class="form-group">
                  <label for="txtImagenUp" class="col-sm-3 control-label">Cambiar imagen:</label>
                  <div class="col-sm-6">
                    <img id="imgActual" src="" alt="Imagen actual">
                  </div>
                  <div class="col-sm-12">
                <br></div>
                <div class="col-sm-3">
                  </div>
                  <div class="col-sm-6 ">
                    <input type="file" id="txtImagenUp">
                  </div>
                </div>
                <br>
                </form> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnActualizar" >Save changes</button>
      </div>
    </div>
  </div>
</div>
  <!-- footer content -->
  <?php 
    require('../menus/footerContent.php');
  ?>
  <!-- /footer content -->

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script type="text/javascript" src="../../assets/js/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../assets/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../assets/js/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../assets/js/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../assets/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../assets/js/demo.js"></script>
<!-- DataTables -->
<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap.min.js"></script>
<!-- Select2 -->
<script src="../../assets/js/select2.full.min.js"></script>
<!-- Capa JS -->
<script src="../js/steps/steps.js"></script>


</body>
</html>
