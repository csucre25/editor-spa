<ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="../panel/dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

        <li><a href="../cliente/cliente"><i class="fa fa-user"></i> <span>Cliente</span></a></li>
        <li><a href="../tarifas/tarifas"><i class="fa fa-shopping-cart"></i> <span>Planes de Pago</span></a></li>
        <li><a href="../beneficios/beneficios"><i class="fa fa-star"></i> <span>Beneficios</span></a></li>
        <li><a href="../steps/steps"><i class="fa fa-object-group"></i> <span>Pasos para Collage</span></a></li>
        <li><a href="../venta/venta"><i class="fa fa-file-text"></i> <span>Ventas</span></a></li>
        <li><a href="../configuration/configuration"><i class="fa fa-cog"></i> <span>Configuracion</span></a></li>


        <!--<li><a href="../gestion/letra.php"><i class="fa  fa-font"></i> <span>Gestión FrontPage</span></a></li>-->


        <!--
        <li class="treeview">
          <a href="#">
            <i class="fa fa-shopping-cart"></i> <span>Ventas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../ventas/cliente.php"><i class="fa fa-circle-o"></i> Clientes</a></li>
            <li><a href="../ventas/ventaCliente.php"><i class="fa fa-circle-o"></i> Realizar Ventas</a></li>
          </ul>
        </li>-->
        <li class="header">REPORTES</li>
        <!--<li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Flujo de Caja</span></a></li>
        
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Pedidos</span></a></li>-->

        <li><a href="../reporte/rletra"><i class="fa fa-circle-o text-yellow"></i> <span>Reporte Consumo</span></a></li>

        <!--<li class="header">SEGURIDAD</li>

        <li><a href="../seguridad/usuario.php"><i class="fa fa-circle-o text-blue"></i> <span>Usuarios</span></a></li>-->

        </ul>
    